r-sat
===

Study of [boolean satisfiability](https://en.wikipedia.org/wiki/Boolean_satisfiability_problem)
 and the [DPLL algorithm](https://en.wikipedia.org/wiki/DPLL_algorithm)
 with rust & wasm (and vue and typescript).

Check out the [website](https://mortaljoe.gitlab.io/r-sat/) for demo and explanation.


Installation
---

Clone repo, install [rust](https://www.rust-lang.org/tools/install).
If you want to also build the `node` project and/or run the website or animation, install
 [wasm](https://rustwasm.github.io/wasm-pack/installer/),
 and [node/npm](https://nodejs.org/en/download/) (or yum or something).

To build r-sat itself, run `cargo build` in the root dir.
To test, run `cargo test`, but keep in mind there is low coverage.
r-sat has a simplistic api, found in `src/api.rs`.
Much of the internals are exposed in `lib.rs` and used or extended as desired.

Once installed, `cargo run` in the main directory will run the `bin.rs` example code, which by
 default generates a sample problem and solves it, printing various elements of problem and solution.
You can generate the plots from the site by running the commented-out `createPlot`
 and `loadAndPlot` instead.
This creates random graphs by default, you will need to modify the `CalculatePlotSatisfiability`
 method in `plot/calc_plot.rs` to generate Barabasi-Albert or Watts–Strogatz graphs,
 or to create and provide your own graph/`Problem` factory.

To build the npm project, first run `wasm-pack build` in the root dir.
This creates a project in `pkg` that can be used in any javascript typescript project.
The api for this project can be found in `wasm/fcn.rs` and the typing in `wasm/typing.rs`.

The animation and website can be built by changing to the `www` dir and running `npm install`.
This will install all necessary packages and only needs to be run once.
The project is (re)built with `npm run build`.
If you tweak the rust project and want to see the result, rerun `wasm-pack build` in the main dir
 and then `npm install ../pkg` in the `www` dir to reinstall just the `r-sat` `rust` project.

The dev site can be demoed with `npm run dev` (with hot reloading).
This host a dev server of the site on http://localhost:8080/r-sat/.
Alternatively, build with `npm run build` and host a production instance at http://localhost:3000/r-sat/.

TLDR:

```
cd $root_dir

# Build rust project
cargo build

# Optional: Run tests
cargo test
# Optional: Run example problem & solution
cargo run

# Build javascript/typescript package of the project
wasm-pack build

# Host dev website
cd www
npm install
npm run dev
```
