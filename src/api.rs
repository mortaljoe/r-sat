use crate::{
  Node, Assignment, Problem, Solver,
  Solution, SolutionLog, DPLLNodeChoiceMode,
  CreateSolver, randomShuffler,
  createSimple3satProblem, createWattsStrogatz3satProblem, createBarabasiAlbert3satProblem
};


fn setupProblem(mut problem: Problem, solvable: bool) -> (Problem, Option<Assignment>) {

  let mut assignment = if solvable {
    Some(Assignment(
      problem.nodes.iter().map(|node| (node.clone(), true)).collect::<Vec<(Node, bool)>>()
    ))
  } else {
    None
  };

  let shuffler = randomShuffler(&problem.nodes);
  problem = shuffler.shuffle(&problem);
  assignment = assignment.and_then(|assignment| Some(shuffler.shuffle(&assignment)));

  (problem, assignment)

}

pub fn setupRandomProblem(N: u32, M: u32, solvable: bool) -> (Problem, Option<Assignment>) {
  setupProblem(createSimple3satProblem(N, M, None, solvable), solvable)
}

pub fn setupWattsStrongatzProblem(
  N: u32, M: u32, solvable: bool, width: u32, rewireChance: f32
) -> (Problem, Option<Assignment>) {
  setupProblem(createWattsStrogatz3satProblem(N, M, None, solvable, width, rewireChance), solvable)
}

pub fn setupBarabasiAlbertProblem(N: u32, M: u32, solvable: bool) -> (Problem, Option<Assignment>) {
  setupProblem(createBarabasiAlbert3satProblem(N, M, None, solvable), solvable)
}

pub fn solveProblem(problem: &Problem) -> Solution {
  let solver = CreateSolver(DPLLNodeChoiceMode::MaxTotClauses);
  solver.solve(&problem)
}

pub fn solveProblemWithLog(problem: &Problem) -> (Solution, SolutionLog) {
  let solver = CreateSolver(DPLLNodeChoiceMode::MaxTotClauses);
  solver.solveWithLog(&problem)
}
