#![allow(non_snake_case)]
#![allow(unused)]

extern crate coopnet;

use coopnet::*;
use std::fs::{create_dir_all, File};
use std::path::Path;
use std::time::{Duration, Instant};


fn solveProblem(problem: &Problem) -> Solution {
  let solver = CreateSolver(DPLLNodeChoiceMode::MaxTotClauses);
  solver.solve(&problem)
}

fn solveProblemWithLog(problem: &Problem) -> (Solution, SolutionLog) {
  let solver = CreateSolver(DPLLNodeChoiceMode::MaxTotClauses);
  solver.solveWithLog(&problem)
}

fn printSolution(problem: &Problem, solution: &Solution) {
  println!("{}", solution);
  if let Some(assignment) = &solution.assignment {
    if assignment.satisfies(problem) {
      println!("Yep, satisfies");
    } else {
      println!("Nope, doesn't");
    }
  }
}

fn simpleTest() {

  // Remember: ~4.3 is the crossover
  let N = 60;
  let M = 258;
  let solvable = false;
  let (problem, assignment) = setupRandomProblem(N, M, solvable);

  println!("{}", problem);
  println!("Expected solution:");
  println!("{}", assignment.map_or(format!("No known solution"), |a| format!("{}", a)));

  let before = Instant::now();
  let solutionWOLog = solveProblem(&problem);
  let durWOLog = Instant::now() - before;
  printSolution(&problem, &solutionWOLog);
  println!("{} s", 0.000000001 * (durWOLog.as_nanos() as f32));

  let before = Instant::now();
  let (solutionWLog, log) = solveProblemWithLog(&problem);
  let durWLog = Instant::now() - before;
  printSolution(&problem, &solutionWLog);
  println!("{} s", 0.000000001 * (durWLog.as_nanos() as f32));

  if solutionWOLog == solutionWLog {
    println!("With & without log matches");
  } else {
    println!("With & without log do not match");
  }

  if solutionWOLog.status != solutionWLog.status {
    panic!("One found solution and another did not!");
  }

}


fn createPlot() {

  let options = SatPlotOptions{
    deltaNumNodes: 20,
    minNumNodes: 20,
    maxNumNodes: 100,
    deltaAlpha: 0.5,
    minAlpha: 0.5,
    maxAlpha: 6.0,
    runsPerPt: 100
  };

  let plots = CalculatePlotSatisfiability(&options);
  let fullPlots = FullSatPlots{ input: options, plots };

  let outDir = Path::new("out");
  create_dir_all(outDir).unwrap();
  let mut saveFile = File::create(outDir.join("satSave.json")).unwrap();
  fullPlots.save(&mut saveFile);

}

#[cfg(all(not(target_arch = "wasm32")))]
fn loadAndPlot() {

  let loadDir = Path::new("out");
  let mut loadFile = File::open(loadDir.join("satSave.json")).unwrap();
  let fullPlots = FullSatPlots::load(&mut loadFile).unwrap();

  PlotPngSat(&fullPlots.plots, &loadDir);
  PlotSvgSat(&fullPlots.plots, &loadDir);

}


fn main() {
  simpleTest();
  // createPlot();
  // loadAndPlot();
}
