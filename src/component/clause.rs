use std::fmt;
use serde::{Serialize, Deserialize};
use super::Literal;


#[derive(Clone, Debug, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct Clause {
  pub id: u32,
  pub lits: Vec<Literal>
}

impl fmt::Display for Clause {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    let fmtstr = self.lits.iter().map(Literal::to_string).collect::<Vec<String>>().join(" V ");
    write!(f, "{}", fmtstr)
  }
}
