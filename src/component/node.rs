use std::fmt;
use serde::{Serialize, Deserialize};


#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct Node {
  pub id: u32
}

impl fmt::Display for Node {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "n{}", self.id)
  }
}


pub type NodeChoice = (Node, bool);


#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct Literal {
  pub node: Node,
  pub sign: bool
}

impl fmt::Display for Literal {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{}{}", if self.sign { " " } else { "~" }, self.node)
  }
}

impl From<NodeChoice> for Literal {
  fn from(tup: (Node, bool)) -> Literal {
    Literal{ node: tup.0, sign: tup.1}
  }
}

impl From<Literal> for NodeChoice {
  fn from(lit: Literal) -> NodeChoice {
    (lit.node, lit.sign)
  }
}
