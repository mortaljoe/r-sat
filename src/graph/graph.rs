extern crate petgraph;

use bimap::{BiMap, hash::LeftValues};
use std::fmt;
use petgraph::{
  data::Create,
  visit::EdgeRef
};
use crate::{Node, Clause};
use crate::Problem;
use super::{SatProp, VertProp};
use super::graph_types as Types;


#[derive(Debug, Clone)]
pub struct SatGraph<Prop: SatProp> {
  graph: Types::PetGraph<Prop>,
  nodeIdxMap: BiMap<Node, Types::NodeId<Prop>>,
  clauseIdxMap: BiMap<Clause, Types::NodeId<Prop>>
}

impl<Prop: SatProp> fmt::Display for SatGraph<Prop> {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    writeln!(f, "Graph: {{")?;
    self.nodeIdxMap.iter().try_fold(
      (), |_, (node, nodeIdx)| writeln!(f, "\tNode: {} -> {}", nodeIdx.index(), node)
    )?;
    self.clauseIdxMap.iter().try_fold(
      (), |_, (clause, clauseIdx)| writeln!(f, "\tClause: {} -> {}", clauseIdx.index(), clause)
    )?;
    self.graph.edge_references().try_fold(
      (), |_, edge| writeln!(f, "\tEdge: {} <-> {}", edge.source().index(), edge.target().index())
    )?;
    writeln!(f, "}}")
  }
}

impl<Prop: SatProp> SatGraph<Prop> {

  pub fn new(nodes: &Vec<Node>, clauses: &Vec<Clause>) -> Self {

    let numVerts = nodes.len() + clauses.len();
    let numEdges = clauses.iter().fold(
      0, |sum, clause| sum + clause.lits.len()
    );

    let mut graph: Types::PetGraph<Prop> = Create::with_capacity(numVerts, numEdges);
    let mut nodeIdxMap = BiMap::new();
    let mut clauseIdxMap = BiMap::new();

    nodes.iter().for_each(|node| {
      let nodeIdx = graph.add_node(VertProp::NProp(Prop::toNodeProp(node)));
      nodeIdxMap.insert(*node, nodeIdx);
    });

    clauses.iter().for_each(|clause| {

      let clauseIdx = graph.add_node(VertProp::CProp(Prop::toClauseProp(clause)));
      clauseIdxMap.insert(clause.clone(), clauseIdx);

      clause.lits.iter().for_each(|lit| {
        match nodeIdxMap.get_by_left(&lit.node) {
          Some(nodeIdx) => { graph.add_edge(clauseIdx, *nodeIdx, Prop::toEdgeProp(clause, lit)); },
          None => panic!("No node {}, from clause {}, inserted")
        }
      })

    });

    SatGraph {
      graph, nodeIdxMap, clauseIdxMap
    }

  }


  pub fn nodes(&self) -> LeftValues<Node, Types::NodeId<Prop>> {
    self.nodeIdxMap.left_values()
  }

  pub fn clauses(&self) -> LeftValues<Clause, Types::NodeId<Prop>> {
    self.clauseIdxMap.left_values()
  }


  // Getter for testing and fragile areas

  pub fn rawGraph(&self) -> &Types::PetGraph<Prop> {
    &self.graph
  }


  // Transform vert idx to Node/Clause

  pub fn vertToNode(&self, vertIdx: &Types::NodeId<Prop>) -> &Node {
    match self.nodeIdxMap.get_by_right(vertIdx) {
      Some(node) => node,
      None => panic!("Cannot find node from vertex")
    }
  }

  pub fn vertToClause(&self, vertIdx: &Types::NodeId<Prop>) -> &Clause {
    match self.clauseIdxMap.get_by_right(vertIdx) {
      Some(clause) => clause,
      None => panic!("Cannot find node from vertex")
    }
  }


  // Transform vert idx to Node/ClauseProp

  pub fn vertToProp(&self, vertIdx: Types::NodeId<Prop>) -> &VertProp<Prop> {
    self.graph.node_weight(vertIdx).unwrap()
  }

  pub fn vertToPropMut(&mut self, vertIdx: Types::NodeId<Prop>) -> &mut VertProp<Prop> {
    self.graph.node_weight_mut(vertIdx).unwrap()
  }

  pub fn vertToNodeProp(&self, vertIdx: Types::NodeId<Prop>) -> &Prop::NodeProp {
    match self.vertToProp(vertIdx) {
      VertProp::<Prop>::NProp(nProp) => nProp,
      VertProp::<Prop>::CProp(_) => panic!("vertIdx corresponds to clause, not node")
    }
  }

  pub fn vertToNodePropMut(&mut self, vertIdx: Types::NodeId<Prop>) -> &mut Prop::NodeProp {
    match self.vertToPropMut(vertIdx) {
      VertProp::<Prop>::NProp(nProp) => nProp,
      VertProp::<Prop>::CProp(_) => panic!("vertIdx corresponds to clause, not node")
    }
  }

  pub fn vertToClauseProp(&self, vertIdx: Types::NodeId<Prop>) -> &Prop::ClauseProp {
    match self.vertToProp(vertIdx) {
      VertProp::<Prop>::NProp(_) => panic!("vertIdx corresponds to node, not clause"),
      VertProp::<Prop>::CProp(cProp) => cProp
    }
  }

  pub fn vertToClausePropMut(&mut self, vertIdx: Types::NodeId<Prop>) -> &mut Prop::ClauseProp {
    match self.vertToPropMut(vertIdx) {
      VertProp::<Prop>::NProp(_) => panic!("vertIdx corresponds to node, not clause"),
      VertProp::<Prop>::CProp(cProp) => cProp
    }
  }


  // Transform Node/Clause to vert idx

  pub fn nodeIdx(&self, node: &Node) -> Types::NodeId<Prop> {
    match self.nodeIdxMap.get_by_left(node) {
      Some(&nodeIdx) => nodeIdx,
      None => panic!("Cannot find node")
    }
  }

  pub fn clauseIdx(&self, clause: &Clause) -> Types::NodeId<Prop> {
    match self.clauseIdxMap.get_by_left(clause) {
      Some(&clauseIdx) => clauseIdx,
      None => panic!("Cannot find clause")
    }
  }


  // Transform Node/Clause to Node/ClauseProp

  pub fn nodeProp(&self, node: &Node) -> &Prop::NodeProp {
    self.vertToNodeProp(self.nodeIdx(node))
  }

  pub fn nodePropMut(&mut self, node: &Node) -> &mut Prop::NodeProp {
    self.vertToNodePropMut(self.nodeIdx(node))
  }

  pub fn clauseProp(&self, clause: &Clause) -> &Prop::ClauseProp {
    self.vertToClauseProp(self.clauseIdx(clause))
  }

  pub fn clausePropMut(&mut self, clause: &Clause) -> &mut Prop::ClauseProp {
    self.vertToClausePropMut(self.clauseIdx(clause))
  }


  // Iterators & filters

  pub fn nodeProps(&self) -> impl Iterator<Item=&Prop::NodeProp> {
    self.nodeIdxMap.right_values().map(move |&nodeIdx| self.vertToNodeProp(nodeIdx))
  }

  pub fn nodePropsIf(&self, pred: fn(&Prop::NodeProp) -> bool) -> impl Iterator<Item=&Prop::NodeProp> {
    self.nodeProps().filter_map(move |nodeProp|
      if pred(&nodeProp) { Some(nodeProp) }
      else { None }
    )
  }

  pub fn nodesIf(&self, pred: fn(&Prop::NodeProp) -> bool) -> impl Iterator<Item=&Node> {
    self.nodeIdxMap.iter().map(
      move |(node, &nodeIdx)| (node, self.vertToNodeProp(nodeIdx))
    ).filter_map(move |(node, nodeProp)|
      if pred(&nodeProp) { Some(node) }
      else { None }
    )
  }


  pub fn clauseProps(&self) -> impl Iterator<Item=&Prop::ClauseProp> {
    self.clauseIdxMap.right_values().map(move |&clauseIdx| self.vertToClauseProp(clauseIdx))
  }

  pub fn clausePropsIf(&self, pred: fn(&Prop::ClauseProp) -> bool) -> impl Iterator<Item=&Prop::ClauseProp> {
    self.clauseProps().filter_map(move |clauseProp|
      if pred(&clauseProp) { Some(clauseProp) }
      else { None }
    )
  }

  pub fn clausesIf(&self, pred: fn(&Prop::ClauseProp) -> bool) -> impl Iterator<Item=&Clause> {
    self.clauseIdxMap.iter().map(
      move |(clause, &clauseIdx)| (clause, self.vertToClauseProp(clauseIdx))
    ).filter_map(move |(clause, clauseProp)|
      if pred(&clauseProp) { Some(clause) }
      else { None }
    )
  }


  pub fn edges(&self, vertIdx: Types::NodeId<Prop>) -> Types::Edges<Prop> {
    self.graph.edges(vertIdx)
  }

  pub fn edgeProp(&self, edgeIdx: Types::EdgeId<Prop>) -> &Prop::EdgeProp {
    self.graph.edge_weight(edgeIdx).unwrap()
  }

  pub fn edgePropMut(&mut self, edgeIdx: Types::EdgeId<Prop>) -> &mut Prop::EdgeProp {
    self.graph.edge_weight_mut(edgeIdx).unwrap()
  }


  pub fn capacity(&self) -> (usize, usize) { self.graph.capacity() }

  pub fn cloneIf(
    &self,
    predNode: fn(&Prop::NodeProp) -> bool,
    predClause: fn(&Prop::ClauseProp) -> bool,
    predEdge: fn(&Prop::EdgeProp) -> bool
  ) -> Self {

    let graph = self.graph.filter_map(
      |_, vertProp| match vertProp {
        VertProp::NProp(nProp) => {
          if predNode(nProp) {
            Some(VertProp::NProp(nProp.clone()))
          } else {
            None
          }
        },
        VertProp::CProp(cProp) => {
          if predClause(cProp) {
            Some(VertProp::CProp(cProp.clone()))
          } else {
            None
          }
        }
      },
      |_, edgeProp| if predEdge(edgeProp) {
        Some(edgeProp.clone())
      } else {
        None
      }
    );
    let mut nodeIdxMap = self.nodeIdxMap.clone();
    nodeIdxMap.retain(|_, idx| predNode(self.vertToNodeProp(*idx)));
    let mut clauseIdxMap = self.clauseIdxMap.clone();
    clauseIdxMap.retain(|_, idx| predClause(self.vertToClauseProp(*idx)));

    SatGraph{ graph, nodeIdxMap, clauseIdxMap }

  }

}


impl<Prop: SatProp> From<&Problem> for SatGraph<Prop> {
  fn from(problem: &Problem) -> Self {
    let nodes = &problem.nodes;
    let clauses = &problem.clauses;
    SatGraph::new(&nodes, &clauses)
  }
}


#[cfg(test)]
pub mod test {
  use super::*;
  use crate::Literal;
  use crate::problem::*;
  use super::super::{EmptySatProp, SatGraphVisitor, BfsSatVisitor};

  pub fn simpleClause() -> Clause {
    Clause{
      id: 0,
      lits: vec!(
        Literal{ node: Node{ id: 0 }, sign: true },
        Literal{ node: Node{ id: 1 }, sign: true },
        Literal{ node: Node{ id: 2 }, sign: true }
      )
    }
  }

  pub fn simpleSatGraph<Prop: SatProp>() -> SatGraph<Prop> {
    let nodes = (0..3).map(|i| Node{ id: i }).collect::<Vec<Node>>();
    let clauses = vec!(simpleClause());
    let problem = Problem{ nodes, clauses };
    SatGraph::from(&problem)
  }


  #[test]
  fn problemGraphCorrectSize() {

    let N = 30;
    let M = 200;
    let nodes = (0..N).map(|i| Node{ id: i }).collect::<Vec<Node>>();

    let shuffler = randomShuffler(&nodes);
    let problem = createSimple3satProblem(N, M, Some(&shuffler), false);
    let satGraph = SatGraph::<EmptySatProp>::from(&problem);

    assert_eq!(problem.nodes.len() + problem.clauses.len(), satGraph.capacity().0);
    assert_eq!(
      problem.clauses.iter().fold(0, |sum, clause| sum + clause.lits.len()),
      satGraph.capacity().1
    );

  }

  #[test]
  fn simpleVisit() {

    let graph = simpleSatGraph::<EmptySatProp>();
    let mut visitor = BfsSatVisitor::new(&graph, &Node{ id: 0 });
    assert_eq!(visitor.next(&graph).unwrap(), graph.nodeIdx(&Node{ id: 0 }));
    assert_eq!(visitor.next(&graph).unwrap(), graph.clauseIdx(&Clause{
      id: 0,
      lits: vec!(
        Literal{ node: Node{ id: 0 }, sign: true },
        Literal{ node: Node{ id: 1 }, sign: true },
        Literal{ node: Node{ id: 2 }, sign: true }
      )
    }));
    let nodeIdxA = visitor.next(&graph).unwrap();
    let nodeIdxB = visitor.next(&graph).unwrap();
    assert!(
      (nodeIdxA == graph.nodeIdx(&Node{ id: 1 }) && nodeIdxB == graph.nodeIdx(&Node{ id: 2 }))
      ||
      (nodeIdxB == graph.nodeIdx(&Node{ id: 1 }) && nodeIdxA == graph.nodeIdx(&Node{ id: 2 }))
    );

  }

}
