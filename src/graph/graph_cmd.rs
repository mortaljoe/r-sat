use crate::{SatGraph, SatProp};
use crate::graph::graph_types as Types;

pub trait GraphCmd<Prop: SatProp> {
  /// Returns the reverse command, to return to original state
  fn apply(&self, satGraph: &mut SatGraph<Prop>) -> Box<GraphCmd<Prop>>;
}


#[derive(Clone, Debug, Eq, PartialEq)]
pub struct SimpleNodeCmd<Prop: SatProp>(pub Types::NodeId<Prop>, pub Prop::NodeProp);
impl<Prop: SatProp> GraphCmd<Prop> for SimpleNodeCmd<Prop> {
  fn apply(&self, satGraph: &mut SatGraph<Prop>) -> Box<GraphCmd<Prop>> {
    let prevProp = satGraph.vertToNodeProp(self.0).clone();
    *satGraph.vertToNodePropMut(self.0) = self.1.clone();
    Box::new(SimpleNodeCmd(self.0.clone(), prevProp))
  }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct SimpleClauseCmd<Prop: SatProp>(pub Types::NodeId<Prop>, pub Prop::ClauseProp);
impl<Prop: SatProp> GraphCmd<Prop> for SimpleClauseCmd<Prop> {
  fn apply(&self, satGraph: &mut SatGraph<Prop>) -> Box<GraphCmd<Prop>> {
    let prevProp = satGraph.vertToClauseProp(self.0).clone();
    *satGraph.vertToClausePropMut(self.0) = self.1.clone();
    Box::new(SimpleClauseCmd(self.0, prevProp))
  }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct SimpleEdgeCmd<Prop: SatProp>(pub Types::EdgeId<Prop>, pub Prop::EdgeProp);
impl<Prop: SatProp> GraphCmd<Prop> for SimpleEdgeCmd<Prop> {
  fn apply(&self, satGraph: &mut SatGraph<Prop>) -> Box<GraphCmd<Prop>> {
    let prevProp = satGraph.edgeProp(self.0).clone();
    *satGraph.edgePropMut(self.0) = self.1.clone();
    Box::new(SimpleEdgeCmd(self.0, prevProp))
  }
}


impl<Prop: SatProp> GraphCmd<Prop> for Box<GraphCmd<Prop>> {
  fn apply(&self, satGraph: &mut SatGraph<Prop>) -> Box<GraphCmd<Prop>> {
    self.as_ref().apply(satGraph)
  }
}

impl<Prop: SatProp, T> GraphCmd<Prop> for Vec<T>
where T: GraphCmd<Prop> {
  fn apply(&self, satGraph: &mut SatGraph<Prop>) -> Box<GraphCmd<Prop>> {
    let mut revVec = vec!();
    for cmd in self {
      revVec.push(cmd.apply(satGraph));
    }
    revVec.reverse();
    Box::new(revVec)
  }
}


#[cfg(test)]
mod test {
  use super::*;

  use std::fmt;
  use crate::{Node, Literal, Clause};
  use crate::graph::test::{simpleClause, simpleSatGraph};

  #[derive(Debug, Clone, Eq, PartialEq)]
  pub struct SimpleProp { }
  impl SatProp for SimpleProp {
    type NodeProp = SimpleNodeProp;
    type ClauseProp = SimpleClauseProp;
    type EdgeProp = SimpleEdgeProp;
    fn toNodeProp(_: &Node) -> Self::NodeProp {
      SimpleNodeProp{ assignment: None }
    }
    fn toClauseProp(_: &Clause) -> Self::ClauseProp {
      SimpleClauseProp{ count: 0 }
    }
    fn toEdgeProp(_: &Clause, lit: &Literal) -> Self::EdgeProp {
      SimpleEdgeProp{ sign: lit.sign }
    }
  }


  #[derive(Debug, Clone, Eq, PartialEq)]
  pub struct SimpleNodeProp {
    pub assignment: Option<bool>
  }
  impl fmt::Display for SimpleNodeProp {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
      write!(f, "SimpleNodeCmd: {}", match self.assignment {
        None => "None",
        Some(assignment) => if assignment { "true" } else { "false" }
      })
    }
  }
  #[derive(Debug, Clone, Eq, PartialEq)]
  pub struct SimpleClauseProp {
    pub count: u32
  }
  impl fmt::Display for SimpleClauseProp {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
      write!(f, "SimpleNodeCmd: {}", self.count)
    }
  }
  #[derive(Debug, Clone, Eq, PartialEq)]
  pub struct SimpleEdgeProp {
    pub sign: bool
  }
  impl fmt::Display for SimpleEdgeProp {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
      write!(f, "SimpleNodeCmd: {}", self.sign)
    }
  }

  fn vertId(id: u32) -> Types::NodeId<SimpleProp> {
    id.into()
  }

  fn edgeId(id: usize) -> Types::EdgeId<SimpleProp> {
    Types::EdgeId::<SimpleProp>::new(id)
  }

  fn assertSameGraphs(g1: &SatGraph<SimpleProp>, g2: &SatGraph<SimpleProp>) {

    for idx in g1.rawGraph().node_indices() {
      let v1 = g1.rawGraph().node_weight(idx).unwrap();
      let v2 = g2.rawGraph().node_weight(idx).unwrap();
      assert_eq!(*v1, *v2);
    }

    for idx in g1.rawGraph().edge_indices() {
      let e1 = g1.rawGraph().edge_weight(idx).unwrap();
      let e2 = g2.rawGraph().edge_weight(idx).unwrap();
      assert_eq!(*e1, *e2);
    }

  }


  #[test]
  fn testSimpleCmds() {

    let mut graph = simpleSatGraph::<SimpleProp>();
    assert_eq!(*graph.nodeProp(&Node{ id: 0}), SimpleNodeProp{ assignment: None });
    assert_eq!(*graph.nodeProp(&Node{ id: 1}), SimpleNodeProp{ assignment: None });
    assert_eq!(*graph.nodeProp(&Node{ id: 1}), SimpleNodeProp{ assignment: None });
    assert_eq!(*graph.clauseProp(&simpleClause()), SimpleClauseProp{ count: 0 });
    let origGraph = graph.clone();

    let newNodeProp = SimpleNodeProp{ assignment: Some(true) };
    let nodeCmd = SimpleNodeCmd(vertId(0), newNodeProp.clone());
    let reverseNodeCmd = nodeCmd.apply(&mut graph);
    assert_eq!(*graph.nodeProp(&Node{ id: 0}), newNodeProp);
    reverseNodeCmd.apply(&mut graph);
    assertSameGraphs(&graph, &origGraph);

    let newClauseProp = SimpleClauseProp{ count: 7 };
    let clauseCmd = SimpleClauseCmd(vertId(3), newClauseProp.clone());
    let reverseClauseCmd = clauseCmd.apply(&mut graph);
    assert_eq!(*graph.clauseProp(&simpleClause()), newClauseProp);

    let newEdgeProp0 = SimpleEdgeProp{ sign: true };
    let newEdgeProp1 = SimpleEdgeProp{ sign: false };
    let edgeCmd0 = SimpleEdgeCmd(edgeId(0), newEdgeProp0.clone());
    let edgeCmd1 = SimpleEdgeCmd(edgeId(1), newEdgeProp1.clone());
    let reverseEdgeCmd0 = edgeCmd0.apply(&mut graph);
    let reverseEdgeCmd1 = edgeCmd1.apply(&mut graph);
    assert_eq!(*graph.edgeProp(edgeId(0)), newEdgeProp0);
    assert_eq!(*graph.edgeProp(edgeId(1)), newEdgeProp1);

    reverseEdgeCmd1.apply(&mut graph);
    reverseClauseCmd.apply(&mut graph);
    reverseEdgeCmd0.apply(&mut graph);

    assertSameGraphs(&graph, &origGraph);

  }

}
