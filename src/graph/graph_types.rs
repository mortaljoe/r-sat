use std::collections::VecDeque;
use fixedbitset::FixedBitSet;
use petgraph::{
  Undirected,
  graph::{UnGraph, Edges as PetEdges, EdgeReference as PetEdgeReference},
  visit::{GraphBase, Visitable, WalkerIter}
};
use crate::{SatProp, VertProp};

pub type PetGraph<Prop> = UnGraph<VertProp<Prop>, <Prop as SatProp>::EdgeProp>;

pub type NodeId<Prop> = <PetGraph<Prop> as GraphBase>::NodeId;
pub type NodeQueue<Prop> = VecDeque<NodeId<Prop>>;

pub type EdgeId<Prop> = <PetGraph<Prop> as GraphBase>::EdgeId;
pub type Edges<'a, Prop> = PetEdges<'a, <Prop as SatProp>::EdgeProp, Undirected>;

pub type EdgeReference<'a, EdgeProp> = PetEdgeReference<'a, EdgeProp>;

pub type VisitMap = FixedBitSet;

pub type Map<Prop> = <PetGraph<Prop> as Visitable>::Map;
pub type Bfs<Prop> = petgraph::visit::Bfs<NodeId<Prop>, Map<Prop>>;
pub type Dfs<Prop> = petgraph::visit::Dfs<NodeId<Prop>, Map<Prop>>;
pub type BfsWIter<'a, Prop> = WalkerIter<Bfs<Prop>, &'a PetGraph<Prop>>;
pub type DfsWIter<'a, Prop> = WalkerIter<Dfs<Prop>, &'a PetGraph<Prop>>;
