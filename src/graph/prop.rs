use std::fmt;
use crate::component::{Node, Literal, Clause};

pub trait PropTraits: fmt::Debug + Clone + Eq + PartialEq + fmt::Display { }
impl<T> PropTraits for T
where T: fmt::Debug + Clone + Eq + PartialEq + fmt::Display { }


pub trait SatProp: fmt::Debug + Clone + Eq + PartialEq + 'static {
  type NodeProp: PropTraits;
  type ClauseProp: PropTraits;
  type EdgeProp: PropTraits;
  fn toNodeProp(node: &Node) -> Self::NodeProp;
  fn toClauseProp(clause: &Clause) -> Self::ClauseProp;
  fn toEdgeProp(clause: &Clause, lit: &Literal) -> Self::EdgeProp;
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum VertProp<Prop: SatProp> {
  NProp(Prop::NodeProp),
  CProp(Prop::ClauseProp)
}
impl<Prop: SatProp> fmt::Display for VertProp<Prop> {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      VertProp::<Prop>::NProp(nProp) => nProp.fmt(f),
      VertProp::<Prop>::CProp(cProp) => cProp.fmt(f)
    }
  }
}


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct EmptyProp { }
impl fmt::Display for EmptyProp {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "EmptySatProp")
  }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct EmptySatProp { }

impl SatProp for EmptySatProp {

  type NodeProp = EmptyProp;
  type ClauseProp = EmptyProp;
  type EdgeProp = EmptyProp;
  fn toNodeProp(_: &Node) -> Self::NodeProp { EmptyProp{ } }
  fn toClauseProp(_: &Clause) -> Self::ClauseProp { EmptyProp{ } }
  fn toEdgeProp(_: &Clause, _: &Literal) -> Self::EdgeProp { EmptyProp{ } }

}
