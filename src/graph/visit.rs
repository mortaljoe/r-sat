use crate::Node;
use super::{SatGraph, SatProp};
use super::graph_types as Types;

/// Similar to petgraph's visitors; see below implementations just use a petgraph visitor internally
pub trait SatGraphVisitor<Prop: SatProp> {
  fn next(&mut self, satGraph: &SatGraph<Prop>) -> Option<Types::NodeId<Prop>>;
}


#[derive(Clone)]
pub struct BfsSatVisitor<Prop: SatProp> {
  rawVisitor: Types::Bfs<Prop>
}

impl<Prop: SatProp> BfsSatVisitor<Prop> {

  pub fn new(satGraph: &SatGraph<Prop>, startNode: &Node) -> Self {
    BfsSatVisitor{
      rawVisitor: Types::Bfs::<Prop>::new(
        &satGraph.rawGraph(),
        satGraph.nodeIdx(startNode)
      )
    }
  }

}

impl<Prop: SatProp> SatGraphVisitor<Prop> for BfsSatVisitor<Prop> {
  fn next(&mut self, satGraph: &SatGraph<Prop>) -> Option<Types::NodeId<Prop>> {
    self.rawVisitor.next(satGraph.rawGraph())
  }
}


#[derive(Clone)]
pub struct DfsSatVisitor<Prop: SatProp> {
  rawVisitor: Types::Dfs<Prop>
}

impl<Prop: SatProp> DfsSatVisitor<Prop> {

  pub fn new(satGraph: &SatGraph<Prop>, startNode: &Node) -> Self {
    DfsSatVisitor{
      rawVisitor: Types::Dfs::<Prop>::new(
        &satGraph.rawGraph(),
        satGraph.nodeIdx(startNode)
      )
    }
  }

}

impl<Prop: SatProp> SatGraphVisitor<Prop> for DfsSatVisitor<Prop> {
  fn next(&mut self, satGraph: &SatGraph<Prop>) -> Option<Types::NodeId<Prop>> {
    self.rawVisitor.next(satGraph.rawGraph())
  }
}
