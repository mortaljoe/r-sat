use std::io::{Read, Write};
use std::sync::mpsc::channel;
use std::thread;

use chrono::Local;
use serde::{Serialize, Deserialize};
use serde_json;

#[allow(unused)]
use crate::{setupRandomProblem, setupBarabasiAlbertProblem, setupWattsStrongatzProblem, solveProblem};
use crate::SolutionStatus;


#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct SatPlotPt {
  pub alpha: f32,
  pub avgSat: f32,
  pub avgTime: f32
}
pub type SatPlot = Vec<SatPlotPt>;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct SatPlotsPt {
  pub numNodes: u32,
  pub plot: SatPlot
}
pub type SatPlots = Vec<SatPlotsPt>;


#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct SatPlotOptions {
  pub deltaNumNodes: u32,
  pub minNumNodes: u32,
  pub maxNumNodes: u32,
  pub deltaAlpha: f32,
  pub minAlpha: f32,
  pub maxAlpha: f32,
  pub runsPerPt: u32
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct FullSatPlots {
  pub plots: SatPlots,
  pub input: SatPlotOptions
}

impl FullSatPlots {
  pub fn save(&self, out: &mut Write) -> serde_json::Result<()> {
    serde_json::to_writer(out, self)
  }
  pub fn load(input: &mut Read) -> serde_json::Result<FullSatPlots> {
    serde_json::from_reader(input)
  }
}


pub fn CalculatePlotSatisfiability(options: &SatPlotOptions) -> SatPlots {

  let startTime = Local::now();
  println!("Plot calculation started at {}", startTime.format("%Y-%m-%d][%H:%M:%S"));

  let SatPlotOptions{
    mut deltaNumNodes, minNumNodes, maxNumNodes,
    mut deltaAlpha, minAlpha, maxAlpha,
    runsPerPt
  } = options.clone();


  let mut plots: SatPlots = Vec::new();

  let sizeNumNodes = (maxNumNodes - minNumNodes) / deltaNumNodes;
  deltaNumNodes = (maxNumNodes - minNumNodes) / sizeNumNodes;
  for numNodes in (0..sizeNumNodes + 1).map(|num| minNumNodes + deltaNumNodes * num) {

    let numNodeTime = Local::now();
    println!("Num nodes: {}", numNodes);

    plots.push(SatPlotsPt{
      numNodes,
      plot: Vec::new()
    });
    let fixedNodePlot = &mut plots.last_mut().unwrap().plot;

    let sizeAlpha = ((maxAlpha - minAlpha) / deltaAlpha).floor() as u32;
    deltaAlpha = (maxAlpha - minAlpha) / (sizeAlpha as f32);
    for alpha in (0..sizeAlpha + 1).map(|alphaNum| minAlpha + deltaAlpha * (alphaNum as f32)) {

      println!("alpha: {}", alpha);

      let numClauses = ((numNodes as f32) * alpha).floor() as u32;

      // Multithread the separate runs
      let (tx, rx) = channel();
      for _ in 0..runsPerPt {
        let tx = tx.clone();
        thread::spawn(move || {
          let problem = setupRandomProblem(numNodes, numClauses, false);
          // let problem = setupBarabasiAlbertProblem(numNodes, numClauses, false);
          // let problem = setupWattsStrongatzProblem(numNodes, numClauses, false, 4, 0.05);
          let before = Local::now();
          let solution = solveProblem(&problem.0);
          let after = Local::now();
          let sat: f32 = if solution.status == SolutionStatus::Satisfied { 1.0 } else { 0.0 };
          let time = 0.000001 * ((after - before).num_microseconds().unwrap() as f32);
          tx.send((sat, time)).unwrap();
        });
      }

      let (totSat, totTime) = (0..runsPerPt).fold(
        (0.0, 0.0),
        |(sat, time), _| {
          let (newSat, newTime) = rx.recv().unwrap();
          (sat + newSat, time + newTime)
        }
      );

      fixedNodePlot.push(SatPlotPt{
        alpha,
        avgSat: totSat / (runsPerPt as f32),
        avgTime: totTime / (runsPerPt as f32)
      })

    }

    let numNodeElapsed = 0.000001 * ((Local::now() - numNodeTime).num_microseconds().unwrap() as f32);
    println!("Elapsed time for {} nodes: {}s", numNodes, numNodeElapsed);

  }

  let endTime = Local::now();
  println!("Plot calculation ended at {}", endTime.format("%Y-%m-%d][%H:%M:%S"));

  let elapsedTime = 0.000001 * ((endTime - startTime).num_microseconds().unwrap() as f32);
  println!("Total elapsed time: {}s", elapsedTime);

  plots

}
