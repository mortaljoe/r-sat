mod calc_plot;
mod plot_sat;

pub use calc_plot::{
  SatPlotPt, SatPlot, SatPlotsPt, SatPlots, FullSatPlots,
  SatPlotOptions, CalculatePlotSatisfiability
};

#[cfg(all(not(target_arch = "wasm32")))]
pub use plot_sat::{PlotPngSat, PlotSvgSat};
#[cfg(target_arch = "wasm32")]
pub use plot_sat::PlotCanvasSat;
