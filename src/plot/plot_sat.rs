#[cfg(all(not(target_arch = "wasm32")))]
use std::fs::create_dir_all;
#[cfg(all(not(target_arch = "wasm32")))]
use std::path::Path;

use plotters::prelude::*;

use super::SatPlots;


const backgroundColor: &RGBColor = &RGBColor(31, 31, 31);
const foregroundColor: &RGBColor = &RGBColor(191, 191, 191);


#[cfg(all(not(target_arch = "wasm32")))]
pub fn PlotPngSat(plots: &SatPlots, outDir: &Path)
-> Result<(), Box<dyn std::error::Error>> {

  create_dir_all(outDir)?;

  let satPlotPath = outDir.join("satPlot.png");
  let timePlotPath = outDir.join("timePlot.png");

  let satRoot = BitMapBackend::new(&satPlotPath, (640, 480)).into_drawing_area();
  satRoot.fill(backgroundColor)?;
  let timeRoot = BitMapBackend::new(&timePlotPath, (640, 480)).into_drawing_area();
  timeRoot.fill(backgroundColor)?;

  PlotSatisfiability(plots, satRoot, timeRoot)

}

#[cfg(all(not(target_arch = "wasm32")))]
pub fn PlotSvgSat(plots: &SatPlots, outDir: &Path)
-> Result<(), Box<dyn std::error::Error>> {

  create_dir_all(outDir)?;

  let satPlotPath = outDir.join("satPlot.svg");
  let timePlotPath = outDir.join("timePlot.svg");

  let satRoot = SVGBackend::new(&satPlotPath, (640, 480)).into_drawing_area();
  satRoot.fill(backgroundColor)?;
  let timeRoot = SVGBackend::new(&timePlotPath, (640, 480)).into_drawing_area();
  timeRoot.fill(backgroundColor)?;

  PlotSatisfiability(plots, satRoot, timeRoot)

}

#[cfg(target_arch = "wasm32")]
pub fn PlotCanvasSat(plots: &SatPlots, satHtmlEl: &str, timeHtmlEl: &str)
-> Result<(), Box<dyn std::error::Error>> {

  let satRoot = CanvasBackend::new(&satHtmlEl).unwrap().into_drawing_area();
  satRoot.fill(backgroundColor)?;
  let timeRoot = CanvasBackend::new(&timeHtmlEl).unwrap().into_drawing_area();
  timeRoot.fill(backgroundColor)?;

  PlotSatisfiability(plots, satRoot, timeRoot)

}


fn PlotSatisfiability<Db: DrawingBackend>(
  plots: &SatPlots,
  satRoot: DrawingArea<Db, plotters::coord::Shift>,
  timeRoot: DrawingArea<Db, plotters::coord::Shift>
) -> Result<(), Box<dyn std::error::Error>>
where <Db as plotters::drawing::backend::DrawingBackend>::ErrorType: 'static {

  let textStyle = TextStyle{
    font: ("Arial", 30).into_font(),
    color: foregroundColor.to_rgba()
  };
  let labelStyle = TextStyle{
    font: ("Arial", 16).into_font(),
    color: foregroundColor.to_rgba()
  };
  let xFormatter = |val: &f32| format!("{}", (val.round() as u32));

  let mut satChart = ChartBuilder::on(&satRoot)
    .caption("3-SAT satisfiability", &textStyle)
    .margin(5)
    .x_label_area_size(50).y_label_area_size(50)
    .build_ranged(0f32..6.1f32, 0f32..1f32)?;
  satChart.configure_mesh()
    .x_desc("M / N").y_desc("% satisfiable")
    .label_style(&labelStyle)
    .disable_x_mesh().disable_y_mesh()
    .x_labels(6).y_labels(2)
    .x_label_formatter(&xFormatter)
    .axis_style(foregroundColor)
    .draw()?;

  let mut timeChart = ChartBuilder::on(&timeRoot)
    .caption("3-SAT log computation time", &textStyle)
    .margin(5)
    .x_label_area_size(50).y_label_area_size(50)
    .build_ranged(0f32..6.1f32, plotters::coord::LogRange(0.1f32..1000f32))?;
  timeChart.configure_mesh()
    .x_desc("M / N").y_desc("log avg computation time")
    .label_style(&labelStyle)
    .disable_x_mesh().disable_y_mesh()
    .x_labels(6).y_labels(5)
    .x_label_formatter(&xFormatter)
    .axis_style(foregroundColor)
    .draw()?;

  for idx in 0..plots.len() {

    let plot = &plots[idx];

    let colorSat = Palette99::pick(idx);
    let label = format!("N={}", plot.numNodes);
    satChart.draw_series(LineSeries::new(
      plot.plot.iter().map(|x| (x.alpha, x.avgSat)), &colorSat
    ))?
      .label(label)
      .legend(move |(x,y)| plotters::element::Path::new(
        vec![(x,y), (x + 20,y)], &colorSat
      ));

    let colorTime = Palette99::pick(idx);
    timeChart.draw_series(LineSeries::new(
      plot.plot.iter().map(|x| (x.alpha, x.avgTime)), &colorTime
    ))?
      .label(format!("N={}", plot.numNodes))
      .legend(move |(x,y)| plotters::element::Path::new(
        vec![(x,y), (x + 20,y)], &colorTime
      ));

  }

  satChart.configure_series_labels()
    .background_style(&backgroundColor.mix(0.8))
    .border_style(foregroundColor)
    .label_font(&labelStyle)
    .position(plotters::chart::SeriesLabelPosition::MiddleLeft)
    .draw()?;

  timeChart.configure_series_labels()
    .background_style(&backgroundColor.mix(0.8))
    .border_style(foregroundColor)
    .label_font(&labelStyle)
    .position(plotters::chart::SeriesLabelPosition::MiddleLeft)
    .draw()?;

  Ok(())

}
