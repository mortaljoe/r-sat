use std::fmt;
use std::ops::{Deref, DerefMut};

use serde::{Serialize, Deserialize};

use crate::component::{NodeChoice, Literal, Clause};
use super::Problem;

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct Assignment(pub Vec<NodeChoice>);

impl Assignment {
  pub fn satisfies(&self, question: &SatisfiedBy) -> bool {
    return question.satisfiedBy(self);
  }
}

impl Deref for Assignment {
  type Target = Vec<NodeChoice>;
  fn deref(&self) -> &Self::Target {
    &self.0
  }
}

impl DerefMut for Assignment {
  fn deref_mut(&mut self) -> &mut Self::Target {
    &mut self.0
  }
}

impl fmt::Display for Assignment {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    let fmtstr = self.iter().map(
      |tup| format!("{}{}", tup.0, if tup.1 { "T" } else { "F" })
    ).collect::<Vec<String>>().join(" ");
    write!(f, "{}", fmtstr)
  }
}


pub trait SatisfiedBy {
  fn satisfiedBy(&self, assignment: &Assignment) -> bool;
}
impl SatisfiedBy for Literal {
  fn satisfiedBy(&self, assignment: &Assignment) -> bool {
    match assignment.iter().find(|pair| pair.0 == self.node) {
      Some(pair) => self.sign == pair.1,
      None => panic!("No node in assignment matches {}", self.node)
    }
  }
}
impl SatisfiedBy for Clause {
  fn satisfiedBy(&self, assignment: &Assignment) -> bool {
    self.lits.iter().any(|lit| lit.satisfiedBy(assignment))
  }
}
impl SatisfiedBy for Problem {
  fn satisfiedBy(&self, assignment: &Assignment) -> bool {
    self.clauses.iter().all(|clause| clause.satisfiedBy(assignment))
  }
}
