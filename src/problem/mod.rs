mod assignment;
mod problem;
mod shuffle;

pub use assignment::{Assignment, SatisfiedBy};
pub use problem::{
  Problem,
  createSimple3satProblem, createWattsStrogatz3satProblem, createBarabasiAlbert3satProblem
};
pub use shuffle::{Shuffler, randomShuffler};
