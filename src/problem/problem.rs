extern crate rand;
extern crate serde_json;

use std::cell::Cell;
use std::fmt;
use rand::{
  Rng,
  seq::{ IteratorRandom, SliceRandom },
  distributions::Standard
};
use serde::{Serialize, Deserialize};

use crate::component::{Node, Literal, Clause};
use crate::util::GetRng;
use super::Shuffler;


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Problem {
  pub nodes: Vec<Node>,
  pub clauses: Vec<Clause>
}

impl fmt::Display for Problem {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    let stringWithQuotes = format!(
      "Problem: {}", serde_json::to_string_pretty(&self).unwrap()
    );
    write!(f, "{}", str::replace(&stringWithQuotes, "\"", ""))
  }
}


fn create3satProblem(
  numNodes: u32, numClauses: u32, optShuffler: Option<&Shuffler>,
  genClause: &mut FnMut(&Vec<Node>, u32) -> Clause
) -> Problem {

  let nodes: Vec<Node> = (0..numNodes).map(|id| Node{ id }).collect();
  let clauses: Vec<Clause> = (0..numClauses).map(|id| genClause(&nodes, id)).collect();

  let problem = Problem{ nodes: nodes.clone(), clauses: clauses };
  match optShuffler {
    Some(shuffler) => shuffler.shuffle(&problem),
    None => problem
  }

}


pub fn createSimple3satProblem(
  numNodes: u32, numClauses: u32, optShuffler: Option<&Shuffler>,
  isSolvable: bool
) -> Problem {

  let genNode = |nodes: &Vec<Node>| nodes.choose_multiple(
    &mut GetRng(), 3
  ).cloned().collect::<Vec<Node>>();
  let genSign = || GetRng().gen_bool(0.5);
  let mut genClause = |nodes: &Vec<Node>, id: u32| {
    let ns = genNode(nodes);
    Clause{
      id: id,
      lits: vec!(
        Literal{ node: ns[0], sign: if isSolvable { true } else { genSign() } },
        Literal{ node: ns[1], sign: genSign() },
        Literal{ node: ns[2], sign: genSign() }
      )
    }
  };

  create3satProblem(numNodes, numClauses, optShuffler, &mut genClause)
  
} 

// Generates 3SAT problem with large clustering, drawing inspiration from
//  the Watts-Strogatz model: generate a near circulant graph then rewire.
// We will generate a 3sat problem with clauses connecting nearby nodes (circulant),
//  then rewire each clause's edges with some probability.
pub fn createWattsStrogatz3satProblem(
  numNodes: u32, numClauses: u32, optShuffler: Option<&Shuffler>,
  isSolvable: bool, widthCirculantConnection: u32, rewireChance: f32
) -> Problem {

  let width = if widthCirculantConnection < 2 { 2 } else { widthCirculantConnection };
  let ratio = (numNodes as f32) / (numClauses as f32);
  let genSign = || GetRng().gen_bool(0.5);

  let mut genClause = |nodes: &Vec<Node>, id: u32| {

    let id1 = ((id as f32) * ratio).floor() as u32;
    let diffs = (0..width).choose_multiple(&mut GetRng(), 2);
    let ns = [
      Cell::new(Node{ id: id1 }),
      Cell::new(Node{ id: id1 + 1 + diffs[0] }),
      Cell::new(Node{ id: id1 + 1 + diffs[1] })
    ];

    ns.iter().for_each(|n| {
      let takeChance: f32 = GetRng().sample(Standard);
      if takeChance < rewireChance {
        n.set(nodes.choose(&mut GetRng()).unwrap().clone());
      }
      while n.get().id >= numNodes {
        n.set(Node{ id: n.get().id - numNodes });
      }
      while ns[0] == ns[1] || ns[0] == ns[2] || ns[1] == ns[2] {
        n.set(nodes.choose(&mut GetRng()).unwrap().clone());
      }
    });

    Clause{
      id: id,
      lits: vec!(
        Literal{ node: ns[0].get(), sign: if isSolvable { true } else { genSign() } },
        Literal{ node: ns[1].get(), sign: genSign() },
        Literal{ node: ns[2].get(), sign: genSign() }
      )
    }

  };

  create3satProblem(numNodes, numClauses, optShuffler, &mut genClause)

}

pub fn createBarabasiAlbert3satProblem(
  numNodes: u32, numClauses: u32, optShuffler: Option<&Shuffler>,
  isSolvable: bool
) -> Problem {

  // Set up a vector of node degrees
  let mut nodeDegrees: Vec<u32> = vec![1; numNodes as usize];
  let mut totDegree = numNodes;
  let genSign = || GetRng().gen_bool(0.5);

  // Generator for Barabasi-Albert is somewhat complicated.
  //  It takes a list of degrees and links clauses to nodes in proportion to (degree+1).
  let mut genClause = |nodes: &Vec<Node>, id: u32| {

    // Select three nodes using the (degree+1) weight
    let mut numLits = totDegree;
    let mut excludedIdxs: Vec<usize> = vec!();
    let ns: Vec<Node> = (0..3).map(|_| {

      // Generate number representing random distance into nodeDegrees
      let chance: f32 = GetRng().sample(Standard);
      let randLit = (chance * (numLits as f32)).floor() as u32;

      // Find node associated with the randLit distance
      let mut passedDegrees = 0;
      let (associatedNodeIdx, associatedDegree) = nodeDegrees.iter_mut().enumerate().skip_while(
        |(idx, &mut degree)| {
          if !excludedIdxs.contains(idx) {
            passedDegrees += degree;
          }
          passedDegrees < randLit
        }
      ).next().unwrap();

      // Node chosen, now increase its degree, add to exclusion, and remove from numLits
      *associatedDegree += 1;
      totDegree += 1;
      excludedIdxs.push(associatedNodeIdx);
      numLits -= nodeDegrees[associatedNodeIdx];

      nodes[associatedNodeIdx]

    }).collect();

    Clause{
      id: id,
      lits: vec!(
        Literal{ node: ns[0], sign: if isSolvable { true } else { genSign() } },
        Literal{ node: ns[1], sign: genSign() },
        Literal{ node: ns[2], sign: genSign() }
      )
    }

  };

  create3satProblem(numNodes, numClauses, optShuffler, &mut genClause)

}



#[cfg(test)]
mod test {
  use super::*;

  use super::super::{Assignment, randomShuffler, SatisfiedBy};

  #[test]
  fn solvableProblemIsSolvable() {

    let N = 30;
    let M = 200;
    let nodes = (0..N).map(|i| Node{ id: i }).collect::<Vec<Node>>();

    let shuffler = randomShuffler(&nodes);
    let problem = createSimple3satProblem(N, M, Some(&shuffler), true);
    let assignment = Assignment(nodes.iter().map(|node|
      shuffler.shuffle(&(node.clone(), true))
    ).collect());

    assert!(problem.satisfiedBy(&assignment));

  }

}
