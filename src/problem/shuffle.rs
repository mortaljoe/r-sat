extern crate rand;

use std::collections::HashMap;
use rand::Rng;
use rand::seq::SliceRandom;

use crate::component::{Node, NodeChoice};
use crate::util::GetRng;


pub struct Shuffler {
  shuffleMap: HashMap<Node, NodeChoice>
}

impl Shuffler {
  pub fn shuffle<T: shuffles::Shuffles>(&self, item: &T) -> T {
    item.shuffle(&self)
  }
}


pub fn randomShuffler(nodes: &Vec<Node>) -> Shuffler {
  let mut shuffledNodes = nodes.clone();
  shuffledNodes.shuffle(&mut GetRng());
  let mut shuffleMap = HashMap::new();
  for (i, node) in nodes.iter().enumerate() {
    shuffleMap.insert(node.clone(), (shuffledNodes[i], GetRng().gen_bool(0.5)));
  }
  Shuffler{ shuffleMap }
}


mod shuffles {
  use super::*;
  use crate::component::{Literal, Clause};
  use super::super::{Assignment, Problem};

  pub trait Shuffles {
    fn shuffle(&self, shuffler: &Shuffler) -> Self;
  }
  impl Shuffles for Node {
    fn shuffle(&self, shuffler: &Shuffler) -> Self {
      shuffler.shuffleMap[self].0
    }
  }
  impl Shuffles for Literal {
    fn shuffle(&self, shuffler: &Shuffler) -> Self {
      match shuffler.shuffleMap.get(&self.node) {
        Some((newNode, keepSign)) => Literal{ node: newNode.clone(), sign: !(self.sign ^ keepSign)},
        None => panic!("Shuffler does not know of node {}", self.node)
      }
    }
  }
  impl Shuffles for NodeChoice {
    fn shuffle(&self, shuffler: &Shuffler) -> Self {
      NodeChoice::from(Literal::from(*self).shuffle(shuffler))
    }
  }
  impl Shuffles for Clause {
    fn shuffle(&self, shuffler: &Shuffler) -> Self {
      Clause{
        id: self.id,
        lits: self.lits.iter().map(|lit| lit.shuffle(shuffler)).collect()
      }
    }
  }
  impl Shuffles for Assignment {
    fn shuffle(&self, shuffler: &Shuffler) -> Self {
      let mut choices = self.0.clone();
      self.iter().for_each(|choice| {
        let shuffledChoice = choice.shuffle(shuffler);
        let newChoice = choices.iter_mut().find(|choice| choice.0 == shuffledChoice.0);
        newChoice.unwrap().1 = shuffledChoice.1;
      });
      Assignment(choices)
    }
  }
  impl Shuffles for Problem {
    fn shuffle(&self, shuffler: &Shuffler) -> Self {
      let nodes = self.nodes.clone();
      let clauses = self.clauses.iter().map(|clause| clause.shuffle(shuffler)).collect();
      Problem{ nodes, clauses }
    }
  }
}



#[cfg(test)]
mod test {
  use super::*;
  use crate::component::{Literal, Clause};
  use super::super::Assignment;

  fn simpleShuffler() -> Shuffler {
    let mut shuffleMap = HashMap::new();
    shuffleMap.insert(Node{ id: 1 }, (Node{ id: 2}, true));
    shuffleMap.insert(Node{ id: 2 }, (Node{ id: 1}, false));
    shuffleMap.insert(Node{ id: 3 }, (Node{ id: 3}, false));
    Shuffler{ shuffleMap }
  }

  fn simpleClause() -> Clause {
    Clause{
      id: 0,
      lits: vec!(
        Literal{ node: Node { id: 1 }, sign: true },
        Literal{ node: Node { id: 2 }, sign: false },
        Literal{ node: Node { id: 3 }, sign: true }
      )
    }
  }

  fn simpleAssignment() -> Assignment {
    Assignment(vec!(
      (Node { id: 1 }, true),
      (Node { id: 2 }, false),
      (Node { id: 3 }, true)
    ))
  }

  #[test]
  fn shuffleNode() {
    let node = Node{ id: 1 };
    let shuffler = simpleShuffler();
    assert_eq!(shuffler.shuffle(&node), Node{ id: 2 });
  }

  #[test]
  fn shuffleLit() {
    let lit = Literal{ node: Node{ id: 1 }, sign: false };
    let shuffler = simpleShuffler();
    assert_eq!(shuffler.shuffle(&lit), Literal{ node: Node{ id: 2 }, sign: false });
  }

  #[test]
  fn shuffleNodeChoice() {
    let choice = (Node{ id: 1 }, false);
    let shuffler = simpleShuffler();
    assert_eq!(shuffler.shuffle(&choice), (Node{ id: 2}, false));
  }

  #[test]
  fn shuffleClause() {
    let clause = simpleClause();
    let shuffler = simpleShuffler();
    let newClause = shuffler.shuffle(&clause);
    assert_eq!(newClause, Clause{
      id: 0,
      lits: vec!(
        Literal{ node: Node{ id: 2 }, sign: true },
        Literal{ node: Node{ id: 1 }, sign: true },
        Literal{ node: Node{ id: 3 }, sign: false }
      )
    });
  }

  #[test]
  fn shuffleAssignment() {
    let assign = simpleAssignment();
    let shuffler = simpleShuffler();
    let newAssign = shuffler.shuffle(&assign);
    assert_eq!(newAssign, Assignment(vec!(
      (Node{ id: 1 }, true),
      (Node{ id: 2 }, true),
      (Node{ id: 3 }, false)
    )));
  }

}
