use crate::{
  Assignment, Node, NodeChoice, Problem, SatGraph
};
use super::super::{Solution, SolutionStatus};
use super::{DPLLProp, DPLLWalker, DPLLStatus, SolutionLog, WalkJournal};

/// DPLLFormula holds solution status for a given satGraph and dictates visitation
pub struct DPLLFormula<'a> {
  problem: &'a Problem,
  satGraph: SatGraph<DPLLProp>,
  journal: WalkJournal
}


impl<'a> DPLLFormula<'a> {

  pub fn new(problem: &Problem, logger: Option<SolutionLog>) -> DPLLFormula {
    DPLLFormula{
      problem: problem,
      satGraph: SatGraph::from(problem),
      journal: WalkJournal::new(logger)
    }
  }

  pub fn chooseNode(&mut self, nodeChoice: NodeChoice) -> Result<(), ()> {

    let mut walker = DPLLWalker::new(&mut self.journal);
    walker.walk(&mut self.satGraph, nodeChoice.0, nodeChoice.1)

  }

  pub fn revertLastChoice(&mut self) {
    if !self.journal.entries.is_empty() {
      self.journal.reverseLastWalk(&mut self.satGraph);
    }
  }

  pub fn currentSolution(&self) -> Solution {
    if self.solved() {
      Solution{
        status: SolutionStatus::Satisfied,
        assignment: Some(Assignment(self.problem.nodes.iter().map(|node|
          (node.clone(), self.satGraph.nodeProp(node).assignment.unwrap())
        ).collect()))
      }
    } else {
      Solution{ status: SolutionStatus::Undetermined, assignment: None }
    }
  }

  pub fn printGraph(&self) {
    let filteredGraph = self.satGraph.cloneIf(
      |node| node.status == DPLLStatus::Active,
      |clause| clause.status == DPLLStatus::Active,
      |edge| edge.status == DPLLStatus::Active
    );
    println!("{}", filteredGraph);
  }

  pub fn solved(&self) -> bool {
    self.indeterminateNodes().next() == None
  }

  pub fn indeterminateNodes(&self) -> impl Iterator<Item=&Node> {
    self.satGraph.nodesIf(|prop| prop.assignment == None)
  }

  pub fn numPosNegActiveEdges(&self, node: &Node) -> (usize, usize) {
    let nodeIdx = self.satGraph.nodeIdx(node).clone();
    self.satGraph.edges(nodeIdx).filter(
      |edge| edge.weight().status == DPLLStatus::Active
    ).fold(
      (0, 0),
      |posNeg, edge| if edge.weight().sign { (posNeg.0 + 1, posNeg.1) } else { (posNeg.0, posNeg.1 + 1) }
    )
  }

  pub fn log(&self) -> &Option<SolutionLog> {
    self.journal.log()
  }

}
