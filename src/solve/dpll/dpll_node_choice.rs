use rand::seq::IteratorRandom;
use crate::NodeChoice;
use crate::util::GetRng;
use super::DPLLFormula;

pub enum DPLLNodeChoiceMode {
	Next, Rand, MaxSameClauses, MaxTotClauses
}

pub type DPLLNodeChooser = fn(&DPLLFormula) -> Option<NodeChoice>;

pub fn CreateNodeChooser(mode: DPLLNodeChoiceMode) -> DPLLNodeChooser {
  match mode {
    DPLLNodeChoiceMode::Next => NextNodeChooser,
    DPLLNodeChoiceMode::Rand => RandNodeChooser,
    DPLLNodeChoiceMode::MaxSameClauses => MaxSameClausesChooser,
    DPLLNodeChoiceMode::MaxTotClauses => MaxTotClausesChooser
  }
}


fn NextNodeChooser(formula: &DPLLFormula) -> Option<NodeChoice> {

  match formula.indeterminateNodes().next() {
    Some(node) => Some((*node, true)),
    None => None
  }

}

fn RandNodeChooser(formula: &DPLLFormula) -> Option<NodeChoice> {

  match formula.indeterminateNodes().choose(&mut GetRng()) {
    Some(node) => Some((*node, true)),
    None => None
  }

}

fn MaxSameClausesChooser(formula: &DPLLFormula) -> Option<NodeChoice> {

  let n = formula.indeterminateNodes().map(
    |node| {
      let posNeg = formula.numPosNegActiveEdges(node);
      if posNeg.0 >= posNeg.1 {
        (node, true, posNeg.0)
      } else {
        (node, false, posNeg.1)
      }
    }
  ).max_by(
    |nodePosNeg1, nodePosNeg2| nodePosNeg1.2.cmp(&nodePosNeg2.2)
  )?;

  Some((*n.0, n.1))

}

fn MaxTotClausesChooser(formula: &DPLLFormula) -> Option<NodeChoice> {

  let n = formula.indeterminateNodes().map(
    |node| {
      let posNeg = formula.numPosNegActiveEdges(node);
      if posNeg.0 >= posNeg.1 {
        (node, true, posNeg.0 + posNeg.1)
      } else {
        (node, false, posNeg.0 + posNeg.1)
      }
    }
  ).max_by(
    |nodePosNeg1, nodePosNeg2| nodePosNeg1.2.cmp(&nodePosNeg2.2)
  )?;

  Some((*n.0, n.1))

}
