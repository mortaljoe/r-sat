use std::fmt;
use crate::{Node, Literal, Clause, SatProp};

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct DPLLProp { }

impl SatProp for DPLLProp {

  type NodeProp = DPLLNodeProp;
  type ClauseProp = DPLLClauseProp;
  type EdgeProp = DPLLEdgeProp;
  fn toNodeProp(_: &Node) -> Self::NodeProp {
    DPLLNodeProp{
      status: DPLLStatus::Active,
      assignment: None
    }
  }
  fn toClauseProp(_: &Clause) -> Self::ClauseProp {
    DPLLClauseProp{
      status: DPLLStatus::Active
    }
  }
  fn toEdgeProp(_: &Clause, lit: &Literal) -> Self::EdgeProp {
    DPLLEdgeProp{
      status: DPLLStatus::Active,
      sign: lit.sign
    }
  }

}


#[derive(Debug, Clone, Eq, PartialEq)]
pub struct DPLLNodeProp {
  pub status: DPLLStatus,
  pub assignment: Option<bool>
}

impl fmt::Display for DPLLNodeProp {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "Status: {}, Assignment: ", self.status)?;
    match self.assignment {
      Some(assignment) => write!(f, "{}", assignment),
      None => write!(f, "None")
    }
  }
}


#[derive(Debug, Clone, Eq, PartialEq)]
pub struct DPLLClauseProp {
  pub status: DPLLStatus
}

impl fmt::Display for DPLLClauseProp {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "Status: {}", self.status)
  }
}


#[derive(Debug, Clone, Eq, PartialEq)]
pub struct DPLLEdgeProp {
  pub status: DPLLStatus,
  pub sign: bool
}

impl fmt::Display for DPLLEdgeProp {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "Status: {}, Sign: {}", self.status, self.sign)
  }
}


#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum DPLLStatus {
  /// Node, Clause, or Edge active
  Active,
  /// Node, Clause, or Edge active
  Inactive
}

impl fmt::Display for DPLLStatus {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match *self {
      DPLLStatus::Active => write!(f, "Active"),
      DPLLStatus::Inactive => write!(f, "Inactive")
    }
  }
}
