use crate::{NodeChoice, Problem};
use super::super::{Solution, SolutionStatus};
use super::super::solver::{Solver, SolverType};
use super::{ChoiceNum, DPLLFormula, SolutionLog};
use super::dpll_node_choice::*;

/// DPLLSolver takes a formula (solution state) and attempts to solve it
pub struct DPLLSolver {
  nodeChooser: DPLLNodeChooser
}

impl DPLLSolver {

  fn solveGeneral(&self, problem: &Problem, logger: Option<SolutionLog>) -> (Solution, Option<SolutionLog>) {

    let mut formula = DPLLFormula::new(problem, logger);
    let mut choiceStack: Vec<(NodeChoice, ChoiceNum)> = vec!();

    while let Some(nodeChoice) = (self.nodeChooser)(&formula) {

      // Choose some nodeChoice, for the first time
      choiceStack.push((nodeChoice, ChoiceNum::FirstChoice));
      let mut success = formula.chooseNode(nodeChoice);

      // If error, start a choice backtrack
      while success.is_err() {
        match choiceStack.pop() {

          // If no choice exists on stack, problem is unsatisfiable
          None => {
            return (
              Solution{
                status: SolutionStatus::Unsatisfiable,
                assignment: None
              },
              formula.log().clone()
            )
          },

          // If some choice, see if first or second choice
          Some((choice, choiceNum)) => {
            match choiceNum {
              // If first choice, try the second choice
              ChoiceNum::FirstChoice => {
                let newChoice = (choice.0, !choice.1);
                choiceStack.push((newChoice, ChoiceNum::SecondChoice));
                success = formula.chooseNode(newChoice);
              },
              // If second choice, unwind to last choice and continue while loop
              ChoiceNum::SecondChoice => {
                formula.revertLastChoice();
              }
            }
          }

        }
      }

    }

    if !formula.solved() {
      panic!("Should not reach here without DPLLFormula being solved")
    }

    (formula.currentSolution(), formula.log().clone())

  }


  pub fn solveWithLog(&self, problem: &Problem) -> (Solution, SolutionLog) {
    let (solution, log) = self.solveGeneral(
      problem,
      Some(SolutionLog::new(problem.nodes.iter(), problem.clauses.iter()))
    );
    (solution, log.unwrap())
  }

}

impl Solver for DPLLSolver {

  const solverType: SolverType = SolverType::Complete;

  fn solve(&self, problem: &Problem) -> Solution {
    self.solveWithLog(problem).0
  }

}


pub fn CreateSolver(mode: DPLLNodeChoiceMode) -> DPLLSolver {
  DPLLSolver{
    nodeChooser: CreateNodeChooser(mode)
  }
}
