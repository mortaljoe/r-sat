use std::collections::VecDeque;
use std::fmt;
use serde::{Serialize, Deserialize};

use crate::{NodeChoice, SatGraph, GraphCmd};
use crate::graph::graph_types as Types;
use super::{DPLLProp, LogAction, SolutionLog, VertOpLog};

pub struct WalkJournal {
  pub entries: Vec<WalkJournalEntry>,
  logger: Option<SolutionLog>
}

impl WalkJournal {

  pub fn new<'a>(logger: Option<SolutionLog>) -> Self {
    WalkJournal{
      entries: vec!(),
      logger
    }
  }

  pub fn currentEntry(&mut self) -> &mut WalkJournalEntry {
    match self.entries.last_mut() {
      Some(entry) => entry,
      None => panic!("No journal entry set")
    }
  }

  pub fn newEntry(&mut self, nodeChoice: NodeChoice) {
    self.entries.push(WalkJournalEntry::new());
    if let Some(logger) = &mut self.logger { logger.newForwardStep(nodeChoice); }
  }

  pub fn reverseLastWalk(&mut self, satGraph: &mut SatGraph<DPLLProp>) {
    match self.entries.pop() {
      None => { panic!("Journal empty; cannot reverse any more"); },
      Some(entry) => {
        entry.reverseStackCmds.apply(satGraph);
        if let Some(logger) = &mut self.logger { logger.reverseLastStep(); }
      }
    }
  }

  pub fn log(&self) -> &Option<SolutionLog> {
    &self.logger
  }


  pub fn logAction(&mut self, action: LogAction) {
    if let Some(logger) = &mut self.logger { logger.logAction(action); }
  }

}


pub struct WalkJournalEntry {
  scheduledOps: VecDeque<VertOp>,
  pub reverseStackCmds: Vec<Box<GraphCmd<DPLLProp>>>
}

impl WalkJournalEntry {

  pub fn new() -> Self {
    WalkJournalEntry{
      scheduledOps: VecDeque::new(),
      reverseStackCmds: Vec::new()
    }
  }

  pub fn scheduleOp(&mut self, op: &VertOp) -> Result<(), ()> {

    match *op {

      VertOp::NodeOp(idx, action) => {
        if let Some(VertOp::NodeOp(_, prevAction)) = self.scheduledOps.iter_mut().find(|op| match op {
          VertOp::NodeOp(prevIdx, _) => idx == *prevIdx,
          VertOp::ClauseOp(_, _) => false
        }) {
          if action != NodeAction::Nothing && *prevAction == NodeAction::Nothing {
            // If new op is assign, replace previous op
            *prevAction = action;
            return Ok(());
          } else if action == NodeAction::SetTrue && *prevAction == NodeAction::SetFalse {
            // If ops conflict, contradiction
            return Err(());
          } else if action == NodeAction::SetFalse && *prevAction == NodeAction::SetTrue {
            // If ops conflict, contradiction
            return Err(());
          } else {
            // Ops are same or new op is nothing; rely on previous op
            return Ok(())
          }
        }
      },

      VertOp::ClauseOp(idx, action) => {
        if let Some(VertOp::ClauseOp(_, prevAction)) = self.scheduledOps.iter_mut().find(|op| match op {
          VertOp::NodeOp(_, _) => false,
          VertOp::ClauseOp(prevIdx, _) => idx == *prevIdx
        }) {
          if action != ClauseAction::Nothing && *prevAction == ClauseAction::Nothing {
            // If new op is prune, replace previous op
            *prevAction = action;
            return Ok(());
          } else {
            // Ops are same or new op is nothing; rely on previous op
            return Ok(())
          }
        }
      }

    }

    // No existing matching op found; schedule op
    self.scheduledOps.push_back(*op);
    Ok(())

  }

  pub fn scheduleOps<'a>(&mut self, ops: impl Iterator<Item=&'a VertOp>) -> Result<(), VertOp> {
    for op in ops {
      match self.scheduleOp(op) {
        Ok(()) => (),
        Err(()) => { return Err(op.clone()); }
      }
    }
    Ok(())
  }

  pub fn popNextOp(&mut self) -> Option<VertOp> {
    self.scheduledOps.pop_front()
  }

}


#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum VertOp {
  NodeOp(Types::NodeId<DPLLProp>, NodeAction),
  ClauseOp(Types::NodeId<DPLLProp>, ClauseAction)
}

impl VertOp {
  pub fn log(&self, satGraph: &SatGraph<DPLLProp>) -> VertOpLog {
    match *self {
      VertOp::NodeOp(idx, action) => VertOpLog::NodeOp(satGraph.vertToNode(&idx).id, action),
      VertOp::ClauseOp(idx, action) => VertOpLog::ClauseOp(satGraph.vertToClause(&idx).id, action)
    }
  }
}

impl fmt::Display for VertOp {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match *self {
      VertOp::NodeOp(nodeIdx, action) => write!(f, "Node operation: node_idx {:?}, action {}", nodeIdx, action),
      VertOp::ClauseOp(clauseIdx, action) => write!(f, "Clause operation: clause_idx {:?}, action {}", clauseIdx, action)
    }
  }
}


#[derive(Clone, Copy, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub enum NodeAction {
  // Nothing
  Nothing,
  // Node should be set to true when visited
  SetTrue,
  // Node should be set to true when visited
  SetFalse,
}

impl fmt::Display for NodeAction {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match *self {
      NodeAction::Nothing => write!(f, "Nothing"),
      NodeAction::SetTrue => write!(f, "SetTrue"),
      NodeAction::SetFalse => write!(f, "SetFalse")
    }
  }
}


#[derive(Clone, Copy, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub enum ClauseAction {
  // Nothing
  Nothing,
  // Clause should be removed when visited
  Remove
}

impl fmt::Display for ClauseAction {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match *self {
      ClauseAction::Nothing => write!(f, "Nothing"),
      ClauseAction::Remove => write!(f, "Remove")
    }
  }
}


#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum ChoiceNum {
  FirstChoice,
  SecondChoice
}



#[cfg(test)]
mod test {
  use super::*;

  fn vertId(id: u32) -> Types::NodeId<DPLLProp> {
    id.into()
  }

  fn assertNodeOpIs(op: Option<VertOp>, idx: u32, action: NodeAction) {
    if let Some(VertOp::NodeOp(nodeIdx, nodeAction)) = op {
      assert_eq!(nodeIdx, vertId(idx));
      assert_eq!(nodeAction, action);
    } else {
      assert!(false);
    }
  }

  fn assertClauseOpIs(op: Option<VertOp>, idx: u32, action: ClauseAction) {
    if let Some(VertOp::ClauseOp(clauseIdx, clauseAction)) = op {
      assert_eq!(clauseIdx, vertId(idx));
      assert_eq!(clauseAction, action);
    } else {
      assert!(false);
    }
  }


  #[test]
  fn testEntry() {

    let mut journal = WalkJournalEntry::new();
    assert!(journal.scheduleOp(&VertOp::NodeOp(vertId(0), NodeAction::Nothing)).is_ok());
    assert!(journal.scheduleOp(&VertOp::ClauseOp(vertId(1), ClauseAction::Remove)).is_ok());
    assert!(journal.scheduleOp(&VertOp::NodeOp(vertId(2), NodeAction::SetFalse)).is_ok());

    assertNodeOpIs(journal.popNextOp(), 0, NodeAction::Nothing);
    assertClauseOpIs(journal.popNextOp(), 1, ClauseAction::Remove);
    assertNodeOpIs(journal.popNextOp(), 2, NodeAction::SetFalse);
    assert_eq!(journal.popNextOp(), None);

    assert!(journal.scheduleOps(vec!(
      VertOp::NodeOp(vertId(0), NodeAction::SetTrue),
      VertOp::ClauseOp(vertId(1), ClauseAction::Nothing),
      VertOp::NodeOp(vertId(2), NodeAction::Nothing)
    ).iter()).is_ok());

    assertNodeOpIs(journal.popNextOp(), 0, NodeAction::SetTrue);
    assertClauseOpIs(journal.popNextOp(), 1, ClauseAction::Nothing);
    assertNodeOpIs(journal.popNextOp(), 2, NodeAction::Nothing);
    assert_eq!(journal.popNextOp(), None);

  }

  #[test]
  fn testReplaceEntry() {

    let mut journal = WalkJournalEntry::new();
    assert!(journal.scheduleOp(&VertOp::NodeOp(vertId(0), NodeAction::Nothing)).is_ok());
    assert!(journal.scheduleOp(&VertOp::NodeOp(vertId(0), NodeAction::SetTrue)).is_ok());

    assertNodeOpIs(journal.popNextOp(), 0, NodeAction::SetTrue);
    assert_eq!(journal.popNextOp(), None);

    assert!(journal.scheduleOp(&VertOp::NodeOp(vertId(0), NodeAction::Nothing)).is_ok());
    assert!(journal.scheduleOp(&VertOp::ClauseOp(vertId(1), ClauseAction::Remove)).is_ok());
    assert!(journal.scheduleOp(&VertOp::NodeOp(vertId(0), NodeAction::Nothing)).is_ok());

    assertNodeOpIs(journal.popNextOp(), 0, NodeAction::Nothing);
    assertClauseOpIs(journal.popNextOp(), 1, ClauseAction::Remove);
    assert_eq!(journal.popNextOp(), None);

  }

}
