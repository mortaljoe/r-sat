use petgraph::visit::EdgeRef;
use crate::{
  Node, SatGraph,
  GraphCmd, SimpleNodeCmd, SimpleClauseCmd, SimpleEdgeCmd
};
use crate::graph::graph_types as Types;
use super::{
  DPLLProp, DPLLStatus, DPLLEdgeProp,
  WalkJournal, VertOp, NodeAction, ClauseAction,
  LogAction
};


pub struct DPLLWalker<'a> {
  /// The log of actions taken over the lifetime of DPLLWalker walks.
  ///  Vec of walk logs
  journal: &'a mut WalkJournal
}

impl<'a> DPLLWalker<'a> {

  /// Create a new visitor and put in the queue of nodes to visit.
  pub fn new(journal: &'a mut WalkJournal) -> Self {
    DPLLWalker{ journal }
  }


  /// Walk the SatGraph using the actionQueue to direct the walk
  pub fn walk(
    &mut self, satGraph: &mut SatGraph<DPLLProp>,
    startNode: Node, startAssign: bool
  ) -> Result<(), ()> {

    let startAction = if startAssign { NodeAction::SetTrue } else { NodeAction::SetFalse };

    self.journal.newEntry((startNode, startAssign));

    self.scheduleOp(
      satGraph,
      &VertOp::NodeOp(satGraph.nodeIdx(&startNode), startAction)
    ).expect("First op scheduled should not fail");

    match self.tryWalk(satGraph) {
      Ok(_) => Ok(()),
      Err(_) => {
        // If error, reverse all commands in this journal entry and drop it
        self.journal.reverseLastWalk(satGraph);
        Err(())
      }
    }

  }

  fn tryWalk(&mut self, satGraph: &mut SatGraph<DPLLProp>) -> Result<(), ()>{
    while let Some(vertOp) = self.journal.currentEntry().popNextOp() {
      self.performOp(satGraph, vertOp)?;
    }
    Ok(())
  }

  /// Schedule vertex operation
  pub fn scheduleOp(&mut self, satGraph: &SatGraph<DPLLProp>, vertOp: &VertOp) -> Result<(), ()> {

    self.journal.logAction(LogAction::ScheduleOp(vertOp.log(satGraph)));
    match self.journal.currentEntry().scheduleOp(vertOp) {
      Ok(()) => Ok(()),
      Err(()) => {
        match vertOp {
          VertOp::NodeOp(idx, _) => {
            self.journal.logAction(LogAction::NodeContradiction(satGraph.vertToNode(idx).id));
          },
          VertOp::ClauseOp(idx, _) => {
            self.journal.logAction(LogAction::ClauseContradiction(satGraph.vertToClause(idx).id));
          }
        }
        Err(())
      }
    }

  }

  /// Perform vertex operation
  fn performOp(&mut self, satGraph: &mut SatGraph<DPLLProp>, vertOp: VertOp)
  -> Result<(), ()> {

    self.journal.logAction(LogAction::PerformOp(vertOp.log(satGraph)));
    match vertOp {

      VertOp::NodeOp(nodeIdx, action) => {

        let tryAssignment = self.tryAssignNode(action, satGraph, nodeIdx);

        if let Some(assignment) = tryAssignment {
          self.propagateNodeAssignmentAndRemoval(satGraph, nodeIdx, assignment)?;
        }

      },

      VertOp::ClauseOp(clauseIdx, action) => {

        if self.shouldRemoveClause(action, satGraph, clauseIdx)? {
          self.propagateClauseRemoval(satGraph, clauseIdx)?;
        }

      },

    }

    Ok(())

  }


  /// Assign node based on prop status and remaining edge signs
  fn tryAssignNode(
    &self,
    action: NodeAction,
    satGraph: &SatGraph<DPLLProp>,
    nodeIdx: Types::NodeId<DPLLProp>
  ) -> Option<bool> {

    // If action explicitly says to set the assignment, do so
    match action {
      NodeAction::SetTrue => { return Some(true); },
      NodeAction::SetFalse => { return Some(false); },
      NodeAction::Nothing => { }
    };

    // If assignment not already set, do some simple checks
    let mut edges = activeEdges(satGraph, nodeIdx);
    let firstEdge = edges.next();
    match firstEdge {
      // First, if no clauses, just set the node sign to true
      None => Some(true),
      Some(edge0) => {
        // At least one edge; now check for all-same-sign clauses connected
        let sign0 = edge0.weight().sign;
        if edges.all(|edge| edge.weight().sign == sign0) {
          Some(sign0)
        } else {
          None
        }
      }
    }

  }

  fn propagateNodeAssignmentAndRemoval(
    &mut self,
    satGraph: &mut SatGraph<DPLLProp>,
    nodeIdx: Types::NodeId<DPLLProp>,
    assignment: bool
  ) -> Result<(), ()> {

    // If assignment was set, send info to connected edges
    for edge in activeEdges(satGraph, nodeIdx) {

      let clauseIdx = edge.target();
      if assignment == edge.weight().sign {
        // If signs match, remove clause
        self.scheduleOp(satGraph, &VertOp::ClauseOp(clauseIdx, ClauseAction::Remove))?;
      } else {
        // If signs do not match, add to action queue to check for unit clause
        self.scheduleOp(satGraph, &VertOp::ClauseOp(clauseIdx, ClauseAction::Nothing))?;
      }

    }

    let assignAndPruneCmd = AssignPruneNodeCmd{
      vertIdx: nodeIdx,
      assignment
    };
    self.journal.logAction(LogAction::PruneAssignNode(satGraph.vertToNode(&nodeIdx).id, assignment));
    self.journal.currentEntry().reverseStackCmds.push(
      assignAndPruneCmd.apply(satGraph)
    );

    Ok(())

  }


  fn shouldRemoveClause(
    &mut self,
    action: ClauseAction,
    satGraph: &SatGraph<DPLLProp>,
    clauseIdx: Types::NodeId<DPLLProp>
  ) -> Result<bool, ()> {

    // If action explicitly says to remove the node, do so
    if action == ClauseAction::Remove {
      return Ok(true);
    }

    // If clause not already removed, do some simple checks
    let mut edges = activeEdges(satGraph, clauseIdx);
    match edges.next() {
      None => {
        // First, if clause is empty, contradiction
        return Err(());
      },
      Some(firstEdge) => {
        // At least one edge
        match edges.next() {
          // Only one edge; remove unit clause and set the node
          None => {
            let action = if firstEdge.weight().sign { NodeAction::SetTrue } else { NodeAction::SetFalse };
            self.scheduleOp(satGraph, &VertOp::NodeOp(firstEdge.target(), action))?;
            Ok(true)
          },
          // More than one; do not remove clause
          Some(_) => Ok(false)
        }
      }
    }

  }

  fn propagateClauseRemoval(
    &mut self,
    satGraph: &mut SatGraph<DPLLProp>,
    clauseIdx: Types::NodeId<DPLLProp>
  ) -> Result<(), ()> {

    // If assignment was set, send info to connected edges
    for edge in activeEdges(satGraph, clauseIdx) {

      let nodeIdx = edge.target();
      // Check node for no-clauses or same-sign clauses
      self.scheduleOp(satGraph, &VertOp::NodeOp(nodeIdx, NodeAction::Nothing))?;
      
    }

    let pruneCmd = PruneClauseCmd{
      vertIdx: clauseIdx
    };
    self.journal.logAction(LogAction::PruneClause(satGraph.vertToClause(&clauseIdx).id));
    self.journal.currentEntry().reverseStackCmds.push(
      pruneCmd.apply(satGraph)
    );

    Ok(())

  }

}


struct AssignPruneNodeCmd {
  vertIdx: Types::NodeId<DPLLProp>,
  assignment: bool
}
impl GraphCmd<DPLLProp> for AssignPruneNodeCmd {
  fn apply(&self, satGraph: &mut SatGraph<DPLLProp>) -> Box<GraphCmd<DPLLProp>> {

    if satGraph.vertToNodeProp(self.vertIdx).status == DPLLStatus::Inactive {
      panic!("Node shouldn't already be inactive!");
    }

    let mut reverseCmds: Vec<Box<GraphCmd<DPLLProp>>> = vec!(Box::new(SimpleNodeCmd(
      self.vertIdx.clone(), satGraph.vertToNodeProp(self.vertIdx).clone()
    )));
    satGraph.vertToNodePropMut(self.vertIdx).assignment = Some(self.assignment);
    satGraph.vertToNodePropMut(self.vertIdx).status = DPLLStatus::Inactive;

    let pruneEdgeCmds = activeEdges(satGraph, self.vertIdx).map(
      move |edge| PruneEdgeCmd{ edgeIdx: edge.id().clone() }
    ).collect::<Vec<PruneEdgeCmd>>();
    for pruneEdgeCmd in pruneEdgeCmds {
      reverseCmds.push(pruneEdgeCmd.apply(satGraph));
    }
    reverseCmds.reverse();

    Box::new(reverseCmds)

  }
}

struct PruneClauseCmd {
  vertIdx: Types::NodeId<DPLLProp>
}
impl GraphCmd<DPLLProp> for PruneClauseCmd {
  fn apply(&self, satGraph: &mut SatGraph<DPLLProp>) -> Box<GraphCmd<DPLLProp>> {

    if satGraph.vertToClauseProp(self.vertIdx).status == DPLLStatus::Inactive {
      panic!("Clause shouldn't already be inactive!");
    }

    let mut reverseCmds: Vec<Box<GraphCmd<DPLLProp>>> = vec!(Box::new(SimpleClauseCmd(
      self.vertIdx.clone(), satGraph.vertToClauseProp(self.vertIdx).clone()
    )));
    satGraph.vertToClausePropMut(self.vertIdx).status = DPLLStatus::Inactive;

    let pruneEdgeCmds = activeEdges(satGraph, self.vertIdx).map(
      move |edge| PruneEdgeCmd{ edgeIdx: edge.id().clone() }
    ).collect::<Vec<PruneEdgeCmd>>();
    for pruneEdgeCmd in pruneEdgeCmds {
      reverseCmds.push(pruneEdgeCmd.apply(satGraph));
    }
    reverseCmds.reverse();

    Box::new(reverseCmds)

  }
}

struct PruneEdgeCmd {
  edgeIdx: Types::EdgeId<DPLLProp>
}
impl GraphCmd<DPLLProp> for PruneEdgeCmd {
  fn apply(&self, satGraph: &mut SatGraph<DPLLProp>) -> Box<GraphCmd<DPLLProp>> {
    let reverseCmd = Box::new(
      SimpleEdgeCmd(self.edgeIdx.clone(), satGraph.edgeProp(self.edgeIdx).clone())
    );
    satGraph.edgePropMut(self.edgeIdx).status = DPLLStatus::Inactive;
    reverseCmd
  }
}

fn activeEdges(satGraph: &SatGraph<DPLLProp>, vertIdx: Types::NodeId<DPLLProp>)
-> impl Iterator<Item=Types::EdgeReference<DPLLEdgeProp>> {
  satGraph.edges(vertIdx).filter(|edge| edge.weight().status == DPLLStatus::Active)
}


#[cfg(test)]
mod test {
  use super::*;
  use super::super::DPLLProp;
  use crate::{Node};
  use crate::graph::test as GTest;


  #[test]
  fn simpleDPLLVisit() {

    let mut satGraph = GTest::simpleSatGraph::<DPLLProp>();
    let mut journal = WalkJournal::new(None);
    let mut walker = DPLLWalker::new(&mut journal);
    assert_eq!(walker.walk(&mut satGraph, Node{ id: 0 }, true), Ok(()));

    assert_eq!(satGraph.nodeProp(&Node{ id: 0 }).assignment, Some(true));
    // assert_eq!(satGraph.nodeProp(&Node{ id: 1 }).assignment, Some(true));
    // assert_eq!(satGraph.nodeProp(&Node{ id: 2 }).assignment, Some(true));

  }

}
