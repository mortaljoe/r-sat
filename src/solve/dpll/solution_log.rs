use std::{
  fmt,
  collections::HashMap
};
use serde::{Serialize, Deserialize};

use crate::{Node, NodeChoice, Clause};
use super::{NodeAction, ClauseAction};


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SolutionLog {
  nodeMap: HashMap<u32, Node>,
  clauseMap: HashMap<u32, Clause>,
  steps: Vec<(NodeChoice, ChoiceLog)>
}

impl SolutionLog {

  pub fn new<'a>(nodes: impl Iterator<Item=&'a Node>, clauses: impl Iterator<Item=&'a Clause>) -> Self {
    SolutionLog{
      nodeMap: nodes.map(|node| (node.id, node.clone())).collect(),
      clauseMap: clauses.map(|clause| (clause.id, clause.clone())).collect(),
      steps: vec!()
    }
  }

  pub fn newForwardStep(&mut self, nodeChoice: NodeChoice) {
    self.steps.push((nodeChoice, ChoiceLog::Forward(vec!())));
  }

  pub fn reverseLastStep(&mut self) {
    let nodeChoice = self.steps.last().unwrap().0;
    self.steps.push((nodeChoice, ChoiceLog::Backward));
  }

  pub fn logAction(&mut self, action: LogAction) {
    match self.steps.last_mut() {
      Some(step) => match &mut step.1 {
        ChoiceLog::Forward(actions) => { actions.push(action); },
        ChoiceLog::Backward => panic!("Cannot log action on a backward ChoiceLog")
      },
      None => panic!("No log recorded")
    }
  }

}

impl fmt::Display for SolutionLog {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    for (id, node) in &self.nodeMap {
      writeln!(f, "{} -> {}", id, node)?;
    }
    for (id, clause) in &self.clauseMap {
      writeln!(f, "{} -> {}", id, clause)?;
    }
    for step in &self.steps {
      let choice = step.0;
      match &step.1 {
        ChoiceLog::Forward(actions) => {
          writeln!(f, "Forward Choice: ({}, {})", choice.0, choice.1)?;
          for action in actions {
            write!(f, "{}", action)?;
          }
        },
        ChoiceLog::Backward => {
          writeln!(f, "Backward Choice: ({}, {})", choice.0, choice.1)?;
        }
      }
    }
    write!(f, "")
  }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum ChoiceLog {
  Forward(Vec<LogAction>),
  Backward
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum LogAction {
  ScheduleOp(VertOpLog),
  PerformOp(VertOpLog),
  PruneAssignNode(u32, bool),
  PruneClause(u32),
  NodeContradiction(u32),
  ClauseContradiction(u32)
}
impl fmt::Display for LogAction {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      LogAction::ScheduleOp(vertOp) => write!(f, "{}", vertOp),
      LogAction::PerformOp(vertOp) => write!(f, "{}", vertOp),
      LogAction::PruneAssignNode(node, action) => writeln!(f, "PruneAssignNode: {}->{}", node, action),
      LogAction::PruneClause(clause) => writeln!(f, "PruneClause: {}", clause),
      LogAction::NodeContradiction(node) => writeln!(f, "NodeContradiction: {}", node),
      LogAction::ClauseContradiction(clause) => writeln!(f, "ClauseContradiction: {}", clause)
    }
  }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum VertOpLog {
  NodeOp(u32, NodeAction),
  ClauseOp(u32, ClauseAction)
}
impl fmt::Display for VertOpLog {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      VertOpLog::NodeOp(node, action) => writeln!(f, "NodeOp: {} {}", node, action),
      VertOpLog::ClauseOp(clause, action) => writeln!(f, "ClauseOp: {} {}", clause, action)
    }
  }
}
