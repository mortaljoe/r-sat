use std::fmt;
use serde::{Serialize, Deserialize};

use crate::Assignment;


#[derive(Clone, Copy, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub enum SolutionStatus {
  Satisfied, Unsatisfiable, Undetermined
}

impl fmt::Display for SolutionStatus {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match *self {
      SolutionStatus::Satisfied => write!(f, "Satisfied"),
      SolutionStatus::Unsatisfiable => write!(f, "Unsatisfiable"),
      SolutionStatus::Undetermined => write!(f, "Undetermined")
    }
  }
}


#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct Solution {
  pub status: SolutionStatus,
  pub assignment: Option<Assignment>
}

impl fmt::Display for Solution {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self.status {
      SolutionStatus::Satisfied => writeln!(f, "Solution satisfied"),
      SolutionStatus::Unsatisfiable => writeln!(f, "Solution unsatisfiable"),
      SolutionStatus::Undetermined => writeln!(f, "Solution undetermined")
    }?;
    match &self.assignment {
      Some(assignment) => write!(f, "{}", assignment),
      None => write!(f, "No solution")
    }
  }
}
