use crate::Problem;
use super::Solution;

pub enum SolverType {
  Complete, Incomplete
}

pub trait Solver {
  const solverType: SolverType;
  fn solve(&self, problem: &Problem) -> Solution;
}
