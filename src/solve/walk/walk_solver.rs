use crate::Problem;
use super::super::{Solution, SolutionStatus};
use super::super::solver::{Solver, SolverType};

pub struct WalkSolver {

}

impl Solver for WalkSolver {
  const solverType: SolverType = SolverType::Incomplete;
  fn solve(&self, _: &Problem) -> Solution {
    Solution{ status: SolutionStatus::Undetermined, assignment: None }
  }
}
