use rand::rngs::OsRng;


pub fn GetRng() -> OsRng {
  OsRng::new().unwrap()
}
