use wasm_bindgen::prelude::*;

use crate::{
  solveProblem, solveProblemWithLog,
  setupRandomProblem, setupWattsStrongatzProblem, setupBarabasiAlbertProblem
};

#[cfg(target_arch = "wasm32")]
use crate::{FullSatPlots, PlotCanvasSat};


#[wasm_bindgen]
pub fn wSetupRandomProblem(N: u32, M: u32) -> JsValue {
  JsValue::from_serde(&setupRandomProblem(N, M, false).0).unwrap()
}

#[wasm_bindgen]
pub fn wSetupSolvableRandomProblem(N: u32, M: u32) -> JsValue {
  JsValue::from_serde(&setupRandomProblem(N, M, true)).unwrap()
}

#[wasm_bindgen]
pub fn wSetupWattsStrongatzProblem(
  N: u32, M: u32, width: u32, rewireChance: f32
) -> JsValue {
  JsValue::from_serde(&setupWattsStrongatzProblem(N, M, false, width, rewireChance).0).unwrap()
}

#[wasm_bindgen]
pub fn wSetupSolvableWattsStrongatzProblem(
  N: u32, M: u32, width: u32, rewireChance: f32
) -> JsValue {
  JsValue::from_serde(&setupWattsStrongatzProblem(N, M, true, width, rewireChance)).unwrap()
}

#[wasm_bindgen]
pub fn wSetupBarabasiAlbertProblem(
  N: u32, M: u32
) -> JsValue {
  JsValue::from_serde(&setupBarabasiAlbertProblem(N, M, false).0).unwrap()
}

#[wasm_bindgen]
pub fn wSetupSolvableBarabasiAlbertProblem(
  N: u32, M: u32
) -> JsValue {
  JsValue::from_serde(&setupBarabasiAlbertProblem(N, M, true)).unwrap()
}

#[wasm_bindgen]
pub fn wSolveProblem(problem: &JsValue) -> JsValue {
  JsValue::from_serde(&solveProblem(&problem.into_serde().unwrap())).unwrap()
}

#[wasm_bindgen]
pub fn wSolveProblemWithLog(problem: &JsValue) -> JsValue {
  JsValue::from_serde(&solveProblemWithLog(&problem.into_serde().unwrap())).unwrap()
}


#[cfg(target_arch = "wasm32")]
#[wasm_bindgen]
pub fn wDrawSatAndTime(fullSatPlots: &JsValue, satEl: &str, timeEl: &str) {
  let plots = &fullSatPlots.into_serde::<FullSatPlots>().unwrap().plots;
  PlotCanvasSat(plots, satEl, timeEl).unwrap();
}
