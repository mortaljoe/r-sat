use cfg_if::cfg_if;
use wasm_bindgen::prelude::*;


cfg_if! {
  // When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
  // allocator.
  if #[cfg(feature = "wee_alloc")] {
    extern crate wee_alloc;
    #[global_allocator]
    static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;
  }
}

#[wasm_bindgen]
pub fn initConsolePanic() {
  console_error_panic_hook::set_once();
}

mod fcn;
mod typing;

pub use fcn::{
  wSetupRandomProblem, wSetupSolvableRandomProblem,
  wSetupWattsStrongatzProblem, wSetupSolvableWattsStrongatzProblem,
  wSetupBarabasiAlbertProblem, wSetupSolvableBarabasiAlbertProblem,
  wSolveProblem, wSolveProblemWithLog
};
pub use typing::*;
