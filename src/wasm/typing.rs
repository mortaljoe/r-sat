use wasm_bindgen::prelude::*;


// Temp typings to help see TS types in the www

#[wasm_bindgen(typescript_custom_section)]
const TS_PROBLEM_EXPORT: &'static str = r"
export interface Problem {
  nodes: Array<{ id: number }>
  clauses: Array<{ id: number, lits: Array<{ node: { id: number }, sign: boolean }> }>
}
";

#[wasm_bindgen(typescript_custom_section)]
const TS_ASSIGNMENT_EXPORT: &'static str = r"
export type Assignment = Array<[{ id: number }, boolean]>
";

#[wasm_bindgen(typescript_custom_section)]
const TS_SOLUTION_STATUS_EXPORT: &'static str = r"
export type SolutionStatus = 'Satisfied' | 'Unsatisfiable' | 'Undetermined'
";

#[wasm_bindgen(typescript_custom_section)]
const TS_SOLUTION_EXPORT: &'static str = r"
export type Solution = { status: SolutionStatus, assignment?: Assignment }
";

#[wasm_bindgen(typescript_custom_section)]
const TS_NODE_OP_EXPORT: &'static str = r"
export type NodeOp = [number, 'Nothing' | 'SetTrue' | 'SetFalse']
";

#[wasm_bindgen(typescript_custom_section)]
const TS_CLAUSE_OP_EXPORT: &'static str = r"
export type ClauseOp = [number, 'Nothing' | 'Prune']
";

#[wasm_bindgen(typescript_custom_section)]
const TS_LOG_ACTION_EXPORT: &'static str = r"
export type LogAction =
  { ScheduleOp: { NodeOp: [number, 'Nothing' | 'SetTrue' | 'SetFalse'] } | { ClauseOp: [number, 'Nothing' | 'Prune']} }
  |
  { PerformOp: { NodeOp: [number, 'Nothing' | 'SetTrue' | 'SetFalse'] } | { ClauseOp: [number, 'Nothing' | 'Prune']} }
  |
  { PruneAssignNode: [number, boolean] }
  |
  { PruneClause: number }
  |
  { NodeContradiction: number }
  |
  { ClauseContradiction: number }
";

#[wasm_bindgen(typescript_custom_section)]
const TS_LOG_STEP_EXPORT: &'static str = r"
export type LogStep = [
  [{ id: number }, boolean],
  {
    Forward: Array<
      { ScheduleOp: { NodeOp: [number, 'Nothing' | 'SetTrue' | 'SetFalse'] } | { ClauseOp: [number, 'Nothing' | 'Prune']} }
      |
      { PerformOp: { NodeOp: [number, 'Nothing' | 'SetTrue' | 'SetFalse'] } | { ClauseOp: [number, 'Nothing' | 'Prune']} }
      |
      { PruneAssignNode: [number, boolean] }
      |
      { PruneClause: number }
      |
      { NodeContradiction: number }
      |
      { ClauseContradiction: number }
    >
  }
  |
  'Backward'
]
";

#[wasm_bindgen(typescript_custom_section)]
const TS_SOLUTION_LOG_EXPORT: &'static str = r"
export type SolutionLog = {
  nodeMap: {
    [id: number]: { id: number }
  },
  clauseMap: {
    [id: number]: { id: number, lits: Array<{ node: { id: number }, sign: boolean }> }
  },
  steps: Array<[
    [{ id: number }, boolean],
    {
      Forward: Array<
        { ScheduleOp: { NodeOp: [number, 'Nothing' | 'SetTrue' | 'SetFalse'] } | { ClauseOp: [number, 'Nothing' | 'Prune']} }
        |
        { PerformOp: { NodeOp: [number, 'Nothing' | 'SetTrue' | 'SetFalse'] } | { ClauseOp: [number, 'Nothing' | 'Prune']} }
        |
        { PruneAssignNode: [number, boolean] }
        |
        { PruneClause: number }
        |
        { NodeContradiction: number }
        |
        { ClauseContradiction: number }
      >
    }
    |
    'Backward'
  ]>
}
";


#[wasm_bindgen(typescript_custom_section)]
const TS_SETUP_RANDOM_PROBLEM_EXPORT: &'static str = r"
export function wSetupRandomProblem(N: number, M: number): Problem
";

#[wasm_bindgen(typescript_custom_section)]
const TS_SETUP_SOLVABLE_RANDOM_PROBLEM_EXPORT: &'static str = r"
export function wSetupSolvableRandomProblem(N: number, M: number): [Problem, Assignment]
";

#[wasm_bindgen(typescript_custom_section)]
const TS_SETUP_WATTS_STRONGATZ_PROBLEM_EXPORT: &'static str = r"
export function wSetupWattsStrongatzProblem(N: number, M: number, width: number, rewireChance: number): Problem
";

#[wasm_bindgen(typescript_custom_section)]
const TS_SETUP_SOLVABLE_WATTS_STRONGATZ_PROBLEM_EXPORT: &'static str = r"
export function wSetupSolvableWattsStrongatzProblem(N: number, M: number, width: number, rewireChance: number): [Problem, Assignment]
";

#[wasm_bindgen(typescript_custom_section)]
const TS_SETUP_BARABASI_ALBERT_PROBLEM_EXPORT: &'static str = r"
export function wSetupBarabasiAlbertProblem(N: number, M: number, width: number, rewireChance: number): Problem
";

#[wasm_bindgen(typescript_custom_section)]
const TS_SETUP_SOLVABLE_BARABASI_ALBERT_PROBLEM_EXPORT: &'static str = r"
export function wSetupSolvableBarabasiAlbertProblem(N: number, M: number, width: number, rewireChance: number): [Problem, Assignment]
";

#[wasm_bindgen(typescript_custom_section)]
const TS_SOLVE_PROBLEM_EXPORT: &'static str = r"
export function wSolveProblem(problem: Problem): Solution
";

#[wasm_bindgen(typescript_custom_section)]
const TS_SOLVE_PROBLEM_EXPORT: &'static str = r"
export function wSolveProblemWithLog(problem: Problem): [Solution, SolutionLog]
";
