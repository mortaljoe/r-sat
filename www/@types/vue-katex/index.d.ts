declare module 'vue-katex' {
  import { PluginObject } from 'vue'
  const VueKatex: PluginObject<undefined>
  export default VueKatex
}
