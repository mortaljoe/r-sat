<template>
<div class="dpll-graph" ref="sigma-container"/>
</template>



<script lang='ts'>
import { Component, Prop, Vue, Watch } from 'vue-property-decorator'
import * as RSat from 'r-sat'
import { sigma } from 'sigma'
import 'sigma/build/plugins/sigma.layout.forceAtlas2.min'

import {
  NodeRenderer, ClauseRenderer, EdgeRenderer, NodeLabelRenderer,
  ClauseStatus, NodeAssignment, NodeAction, ClauseAction
} from './Renderers'


interface IGraphChange {
  vert: SigmaJs.Node,
  field: string,
  old: any,
  new: any
}

enum AnimStatus {
  Off,
  Running,
  Paused
}


@Component
export default class AnimGraph extends Vue {

  private static nodeSize: number = 1.5
  private static clauseSize: number = 1

  private static coolingTime: number = 5000
  private stopTimer: number | null = null

  @Prop()
  private problem: RSat.Problem
  @Prop({ default: 5 })
  private decisionFrameTime: number
  @Prop({ default: 5 })
  private forwardFrameTime: number
  @Prop({ default: 2 })
  private backwardFrameTime: number

  private sigmaInstance: sigma
  public nodeChoices: Array<[number, boolean]> = []

  private currentAnimGen: IterableIterator<number> | null = null
  private animStatus: AnimStatus = AnimStatus.Off

  public get isAnimationRunning() { return this.animStatus === AnimStatus.Running }
  public get isAnimationPaused() { return this.animStatus === AnimStatus.Paused }

  @Watch('problem')
  private onProblemChanged() {
    this.createGraph()
  }


  private createGraph() {

    if (this.sigmaInstance) this.sigmaInstance.kill()
    this.nodeChoices = []
    this.currentAnimGen = null
    this.animStatus = AnimStatus.Off

    // Let's first initialize sigma:
    this.sigmaInstance = new this.$sigma()

    // Add renderer for nodes, clauses, edges
    const renderer = this.sigmaInstance.addRenderer({
      container: this.$refs['sigma-container'] as Element,
      type: 'canvas'
    })
    this.sigmaInstance.settings({
      defaultLabelColor: '#000',
      labelSize: 'ratio',
      labelSizeRatio: '1.5'
    })

    // Add a graph node for each node
    const numNodes = this.problem.nodes.length
    for (const [idx, node] of this.problem.nodes.entries()) {

      // If node is not connected, don't include in the graph
      if (this.problem.clauses.some(clause => {
        return clause.lits.some(lit => lit.node.id === node.id)
      })) {

        const id = 'n' + node.id
        this.sigmaInstance.graph.addNode({
          id,
          label: String(node.id),
          x: 100 * Math.cos(2 * idx * Math.PI / numNodes),
          y: 100 * Math.sin(2 * idx * Math.PI / numNodes),
          size: AnimGraph.nodeSize,
          type: 'nodeRenderer',
          action: null,
          assignment: NodeAssignment.None
        })

      }

    }

    // Add a graph node for each clause and add edges to all nodes in the clause
    const numClauses = this.problem.clauses.length
    for (const [idx, clause] of this.problem.clauses.entries()) {
      const id = 'c' + idx
      this.sigmaInstance.graph.addNode({
        id,
        label: clause.lits.map(lit => (lit.sign ? '' : '~') + lit.node.id).join(' ^ '),
        x: 50 * Math.cos(2 * idx * Math.PI / numClauses),
        y: 50 * Math.sin(2 * idx * Math.PI / numClauses),
        size: AnimGraph.clauseSize,
        type: 'clauseRenderer',
        action: null,
        status: ClauseStatus.Active,
        settings: {
          drawLabels: false
        }
      })
      for (const lit of clause.lits) {
        const nodeId = 'n' + lit.node.id
        this.sigmaInstance.graph.addEdge({
          id: id + '-' + nodeId,
          source: id,
          target: nodeId,
          type: 'edgeRenderer',
          sign: lit.sign
        })
      }
    }

    this.startTimedForceAtlas()

  }

  public colorAssignmentFromSolution(solution: RSat.Solution) {
    if (solution.status === 'Satisfied') {
      (solution.assignment as RSat.Assignment).forEach(nodeChoice => {
        const id = 'n' + nodeChoice[0].id
        this.sigmaInstance.graph.nodes(id).assignment = nodeChoice[1] ? NodeAssignment.True : NodeAssignment.False
      })
      this.sigmaInstance.refresh()
    }
  }


  public resetGraph() {
    this.resetAssignment()
    this.resetClauses()
  }

  public resetAssignment() {
    this.sigmaInstance.graph.nodes().filter(
      vert => vert.id.startsWith('n')
    ).forEach(
      node => { node.assignment = NodeAssignment.None }
    )
    this.sigmaInstance.refresh()
  }

  public resetClauses() {
    this.sigmaInstance.graph.nodes().filter(
      vert => vert.id.startsWith('c')
    ).forEach(
      clause => { clause.status = ClauseStatus.Active }
    )
    this.sigmaInstance.refresh()
  }


  public startTimedForceAtlas() {

    // Start force atlas
    this.sigmaInstance.startForceAtlas2()
    this.sigmaInstance.refresh()

    if (this.stopTimer) window.clearTimeout(this.stopTimer)
    this.stopTimer = window.setTimeout(() => this.sigmaInstance.stopForceAtlas2(), AnimGraph.coolingTime)

  }

  public stopTimedForceAtlas() {
    this.sigmaInstance.stopForceAtlas2()
    this.stopTimer = null
  }



  public animateSolution(solutionLog: RSat.SolutionLog) {
    this.resetGraph()
    this.currentAnimGen = this.animationGenerator(this, solutionLog)
    this.resumeAnimation()
  }

  public resumeAnimation() {
    this.animStatus = AnimStatus.Running
    const animTimer = window.setTimeout(
      this.animTimeout,
      this.decisionFrameTime
    )
  }

  public pauseAnimation() {
    switch (this.animStatus) {
    case AnimStatus.Off:
      throw Error('No animation to pause')
    case AnimStatus.Running:
      this.animStatus = AnimStatus.Paused
      break
    case AnimStatus.Paused:
      break
    }
  }

  private animTimeout() {
    this.sigmaInstance.refresh()
    if (this.currentAnimGen != null) {
      switch (this.animStatus) {
      case AnimStatus.Off:
      case AnimStatus.Paused:
        break
      case AnimStatus.Running:
        const current = this.currentAnimGen.next()
        if (current.done) {
          this.animStatus = AnimStatus.Off
        } else {
          window.setTimeout(this.animTimeout, current.value)
        }
      }
    }
  }


  private animationGenerator = function*(
    self: AnimGraph,
    solutionLog: RSat.SolutionLog
  ): IterableIterator<number> {

    if (solutionLog != null) {

      const nodeMap = solutionLog.nodeMap
      const clauseMap = solutionLog.clauseMap
      const graphChanges: IGraphChange[][] = []

      let step: RSat.LogStep
      for (step of solutionLog.steps) {
        const nodeChosen = step[0][0].id
        const choice = step[0][1]

        if (step[1] === 'Backward') {

          self.$emit('revertChoice', nodeChosen, choice)
          yield self.decisionFrameTime

          const lastChanges = graphChanges.pop()
          if (!lastChanges)
            throw Error('Backward step attempted with no graph changes left')

          for (const change of lastChanges.reverse()) {
            change.vert[change.field] = change.old
            yield self.backwardFrameTime
          }

        } else {

          self.$emit('chooseNode', nodeChosen, choice)
          yield self.decisionFrameTime

          self.nodeChoices.push([nodeChosen, choice])
          graphChanges.push([])

          for (const action of step[1].Forward) {

            if (self.isScheduleVertOp(action)) {
              const op = action.ScheduleOp

              if (self.isNodeOp(op)) {

                const node = nodeMap[op.NodeOp[0]].id
                const sigId = 'n' + node
                const sigNode = self.sigmaInstance.graph.nodes(sigId)

                const oldAction = sigNode.action
                if (op.NodeOp[1] === 'Nothing') {
                  sigNode.action = NodeAction.Nothing
                } else if (op.NodeOp[1] === 'SetTrue') {
                  sigNode.action = NodeAction.SetTrue
                } else if (op.NodeOp[1] === 'SetFalse') {
                  sigNode.action = NodeAction.SetFalse
                }
                graphChanges[graphChanges.length - 1].push({
                  vert: sigNode, field: 'action', old: oldAction, new: sigNode.action
                })

              } else if (self.isClauseOp(op)) {

                const clause = clauseMap[op.ClauseOp[0]].id
                const sigId = 'c' + clause
                const sigClause = self.sigmaInstance.graph.nodes(sigId)

                const oldAction = sigClause.action
                if (op.ClauseOp[1] === 'Nothing') {
                  sigClause.action = ClauseAction.Nothing
                } else if (op.ClauseOp[1] === 'Prune') {
                  sigClause.action = ClauseAction.Prune
                }
                graphChanges[graphChanges.length - 1].push({
                  vert: sigClause, field: 'action', old: oldAction, new: sigClause.action
                })

              }

            } else if (self.isPerformVertOp(action)) {
              const op = action.PerformOp

              if (self.isNodeOp(op)) {

                const node = nodeMap[op.NodeOp[0]].id
                const sigId = 'n' + node
                const sigNode = self.sigmaInstance.graph.nodes(sigId)

                const oldAction = sigNode.action
                sigNode.action = null
                graphChanges[graphChanges.length - 1].push({
                  vert: sigNode, field: 'action', old: oldAction, new: sigNode.action
                })

              } else if (self.isClauseOp(op)) {

                const clause = clauseMap[op.ClauseOp[0]].id
                const sigId = 'c' + clause
                const sigClause = self.sigmaInstance.graph.nodes(sigId)

                const oldAction = sigClause.action
                sigClause.action = null
                graphChanges[graphChanges.length - 1].push({
                  vert: sigClause, field: 'action', old: oldAction, new: sigClause.action
                })

              }

            } else if (self.isPruneAssignNode(action)) {

              const node = nodeMap[action.PruneAssignNode[0]].id
              const sign = action.PruneAssignNode[1]
              // Assign node, remove edges
              const sigId = 'n' + node
              const sigNode = self.sigmaInstance.graph.nodes(sigId)

              const oldAssignment = sigNode.assignment
              sigNode.assignment = sign ? NodeAssignment.True : NodeAssignment.False
              graphChanges[graphChanges.length - 1].push({
                vert: sigNode, field: 'assignment', old: oldAssignment, new: sigNode.assignment
              })

            } else if (self.isPruneClause(action)) {

              const clause = clauseMap[action.PruneClause].id
              // Prune clause, remove edges
              const sigId = 'c' + clause
              const sigClause = self.sigmaInstance.graph.nodes(sigId)

              const oldStatus = sigClause.status
              sigClause.status = ClauseStatus.Inactive
              graphChanges[graphChanges.length - 1].push({
                vert: sigClause, field: 'status', old: oldStatus, new: sigClause.status
              })

            } else if (self.isNodeContradiction(action)) {

              const node = nodeMap[action.NodeContradiction]
              // Flash node

            } else if (self.isClauseContradiction(action)) {

              const clause = clauseMap[action.ClauseContradiction]
              // Flash clause

            }

            yield self.forwardFrameTime

          }

        }

      }

    }
  }

  private isScheduleVertOp(action: RSat.LogAction):
  action is { ScheduleOp: { NodeOp: RSat.NodeOp } | { ClauseOp: RSat.ClauseOp } } {
    return (action as { ScheduleOp: { NodeOp: RSat.NodeOp } | { ClauseOp: RSat.ClauseOp } }).ScheduleOp !== undefined
  }
  private isPerformVertOp(action: RSat.LogAction):
  action is { PerformOp: { NodeOp: RSat.NodeOp } | { ClauseOp: RSat.ClauseOp } } {
    return (action as { PerformOp: { NodeOp: RSat.NodeOp } | { ClauseOp: RSat.ClauseOp } }).PerformOp !== undefined
  }
  private isNodeOp(vertOp: { NodeOp: RSat.NodeOp } | { ClauseOp: RSat.ClauseOp }):
  vertOp is { NodeOp: RSat.NodeOp } {
    return (vertOp as { NodeOp: RSat.NodeOp }).NodeOp !== undefined
  }
  private isClauseOp(vertOp: { NodeOp: RSat.NodeOp } | { ClauseOp: RSat.ClauseOp }):
  vertOp is { ClauseOp: RSat.ClauseOp } {
    return (vertOp as { ClauseOp: RSat.ClauseOp }).ClauseOp !== undefined
  }
  private isPruneAssignNode(action: RSat.LogAction):
  action is { PruneAssignNode: [number, boolean] } {
    return (action as { PruneAssignNode: [number, boolean] }).PruneAssignNode !== undefined
  }
  private isPruneClause(action: RSat.LogAction):
  action is { PruneClause: number } {
    return (action as { PruneClause: number }).PruneClause !== undefined
  }
  private isNodeContradiction(action: RSat.LogAction):
  action is { NodeContradiction: number } {
    return (action as { NodeContradiction: number }).NodeContradiction !== undefined
  }
  private isClauseContradiction(action: RSat.LogAction):
  action is { ClauseContradiction: number } {
    return (action as { ClauseContradiction: number }).ClauseContradiction !== undefined
  }



  public mounted() {

    this.$sigma.canvas.nodes.nodeRenderer = NodeRenderer
    this.$sigma.canvas.nodes.clauseRenderer = ClauseRenderer
    this.$sigma.canvas.edges.edgeRenderer = EdgeRenderer
    this.$sigma.canvas.labels.nodeRenderer = NodeLabelRenderer

    this.createGraph()

  }

  public beforeUpdate() {
    this.sigmaInstance.refresh()
  }

  public beforeDestroy() {
    // Kill force atlas before stopping
    this.sigmaInstance.killForceAtlas2()
  }

}
</script>



<style scoped>
.dpll-graph {
  left: 0;
  height: 800px;
}
</style>
