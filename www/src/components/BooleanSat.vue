<template>
<div>

  <h1>Boolean Satisfiability (<a href="https://en.wikipedia.org/wiki/Boolean_satisfiability_problem">wiki</a>)</h1>

  <h2>The problem</h2>

  <p>The problem of boolean satisfiability is simple: can a set of statements all be satisfied simultaneously?
    Say you have a set of <katex-element expression="N"/> "literals" (expressions that can be either true or false,
    e.g. "It is raining"). Say you combine these statements into <katex-element expression="M"/> "clauses" with
    the OR and NOT operations ("It is raining OR my cat is frightened OR the door is NOT open"). The boolean
    satisfiability problem asks if all clauses can be true at the same time.</p>

  <p>Consider two literals: "It is raining" and "my cat is frightened". Is there some universe in which
    "It is raining OR my cat is frightened" can be true? Yes, easily, either it is raining or my cat is frightened.
    What about two clauses: "It is raining OR my cat is frightened" AND "It is raining OR my cat is NOT frightened"?
    Yes, if it is raining then both of these clauses are true. Three clauses?</p>

  <div v-katex:display="
    '\\left(\\textrm{raining} \\lor \\textrm{frightened}\\right)' + '\\land' +
    '\\left(\\textrm{raining} \\lor \\neg\\textrm{frightened}\\right)' + '\\land' +
    '\\left(\\neg\\textrm{raining} \\lor \\neg\\textrm{frightened}\\right)' +
    '\\in \\textrm{SAT}'
  "></div>

  <p>This is <em>still</em> satisfiable, by setting
    <katex-element expression="\left(\textrm{raining}=T\right)"/> and
    <katex-element expression="\left(\textrm{frightened}=F\right)"/>.
    But it is impossible to simultaneously satisfy all four clause possibilities together.</p>

  <div v-katex:display="
    '\\left(\\textrm{raining} \\lor \\textrm{frightened}\\right)' + '\\land' +
    '\\left(\\textrm{raining} \\lor \\neg\\textrm{frightened}\\right)' + '\\land' +
    '\\left(\\neg\\textrm{raining} \\lor \\textrm{frightened}\\right)' + '\\land' +
    '\\left(\\neg\\textrm{raining} \\lor \\neg\\textrm{frightened}\\right)' +
    '\\in \\textrm{UNSAT}'
  "/>

  <p>This is the boolean satisfiability (or "SAT") problem: is it possible to satisfy all clauses?</p>

  <h2>Importance & abstraction</h2>

  <p>This is a surprisingly important problem to analyze,
    given its concrete nature compared to many computer science problems. It is rather difficult
    to solve, being one of the hardest problems to solve in a difficulty class known as
    "nondeterministic polynomical time" or "<a href="https://en.wikipedia.org/wiki/NP_(complexity)">NP</a>".
    (Note that the problem is <em>not</em> to find or count all satisfying solutions,
    a harder problem known as "#SAT").</p>

  <p>Roughly, this means (1) if given an attempted solution, you can quickly verify if it is or is not
    a solution and (2) you cannot quickly <em>find</em> a solution
    (<a href="https://en.wikipedia.org/wiki/P_versus_NP_problem">probably...</a>).
    The SAT problem was the first problem to be proven as
    <a href="https://en.wikipedia.org/wiki/NP-completeness">NP-complete</a>, which means it is both
    in the NP difficulty class and is at least as hard to solve as any other problem that is also NP.
    Because of this, research into the SAT problem can reveal much about a whole class of difficult,
    completely different problems.</p>

  <p>Normally, the actual literals are abstracted away from their concrete meaning, e.g. to variables:</p>
  <div v-katex:display="'x_0=\\textrm{raining}, \\quad x_1=\\textrm{frightened}'"/>
  <div v-katex:display="
    '(x_0 \\lor x_1) \\land (x_0 \\lor \\neg x_1)' +
    '\\land (\\neg x_0 \\lor \\neg x_1) \\in \\textrm{SAT}'
  "/>
  <div v-katex:display="
    '(x_0 \\lor x_1) \\land (x_0 \\lor \\neg x_1)' +
    '\\land (x_0 \\lor \\neg x_1) \\land (\\neg x_0 \\lor \\neg x_1) \\in \\textrm{UNSAT}'
  "/>

  <p>On the DPLLGraph tab, I've abstracted the literals and clauses into a graph, literals as circles
    and clauses as squares. There is an edge between circle and square if the literal is in the clause,
    blue if it is just in the clause by itself and red if it is negated.</p>

  <div v-katex:display="
    '(x_0 \\lor x_1 \\lor \\neg x_2) \\land (\\neg x_0 \\lor \\neg x_2 \\lor x_3)'
  "/>

  <div class="example-graph" ref="sigma-container"/>
    
  <p>I restrict to the 3SAT problem,
    which means the SAT problem where each clause contains exactly three literals. This lets us get a grasp
    on the problem without losing generality, since 3SAT is by itself NP-complete (so solutions of 3SAT
    are reflective of the general SAT problems and NP problems more broadly; 2SAT is not
    NP-complete).</p>

  <h2>Average satisfiability</h2>

  <p>One note before discussing the solution algorithm; consider the <em>average</em> satisfiability
    of a 3SAT problem. If we fix the ratio of number of clauses, <katex-element expression="M"/>,
    to number of nodes, <katex-element expression="N"/>, to be <katex-element expression="\alpha=M/N"/>,
    and we increase the number of literals and clauses together,
    random problems tend toward all being satisfiable or unsatisfiable as the numbers get large.</p>
    
  <p>More literals means more freedom to have a solution, so small <katex-element expression="\alpha"/>
    means a problem is likely solvable. More clauses means more constraints on a solution, so the converse is true.
    The tipping point between the two behaviors is at <katex-element expression="\alpha\sim4.3"/>.
    This is why the site is initially tuned to <katex-element expression="N=40"/> and
    <katex-element expression="M=172"/>. <katex-element expression="172/40\approx4.3"/> and you will see
    it is sometimes satisfiable and sometimes not.</p>

  <p>The following graph shows the crossover between usually SAT to usually UNSAT at <katex-element expression="\alpha\sim4.3"/>.
    Also note that the transition is sharper as the number of nodes and clauses increases. This is a
    critical phase transition between the states, and phase transitions crisp as the system size increases.</p>

  <!-- Not sure the 'onerror' works correctly -->
  <img :src="satSvg" :onerror="this.src=satPng" class="plot"/>

</div>
</template>



<script lang="ts">
import { Component, Vue } from 'vue-property-decorator'

import { sigma } from 'sigma'

import {
  NodeRenderer, ClauseRenderer, EdgeRenderer, NodeLabelRenderer,
  ClauseStatus, NodeAssignment
} from './Renderers'

import satSvg from '@/assets/satPlot.svg'
import satPng from '@/assets/satPlot.png'


@Component({})
export default class BooleanSat extends Vue {

  private static nodeSize: number = 1.5
  private static clauseSize: number = 1

  private sigmaInstance: sigma

  private get satSvg() { return satSvg }
  private get satPng() { return satPng }


  private createGraph() {

    if (this.sigmaInstance) this.sigmaInstance.kill()

    // Let's first initialize sigma:
    this.sigmaInstance = new this.$sigma()

    // Add renderer for nodes, clauses, edges
    const renderer = this.sigmaInstance.addRenderer({
      container: this.$refs['sigma-container'] as Element,
      type: 'canvas'
    })
    this.sigmaInstance.settings({
      defaultLabelColor: '#000',
      labelSize: 'ratio',
      labelSizeRatio: '1.5'
    })

    // Add nodes
    for (let idx = 0; idx < 4; ++idx) {
      const id = 'n' + idx
      this.sigmaInstance.graph.addNode({
        id,
        label: String(idx),
        x: 100 * Math.cos(2 * idx * Math.PI / 4),
        y: 100 * Math.sin(2 * idx * Math.PI / 4),
        size: BooleanSat.nodeSize,
        type: 'nodeRenderer',
        action: null,
        assignment: NodeAssignment.None
      })
    }

    // Add clauses and edges
    const id0 = 'c0'
    this.sigmaInstance.graph.addNode({
      id: id0,
      label: 'n0 ^ n1 ^ ~n2',
      x: 30,
      y: 30,
      size: BooleanSat.clauseSize,
      type: 'clauseRenderer',
      action: null,
      status: ClauseStatus.Active,
      settings: {
        drawLabels: false
      }
    })
    const id1 = 'c1'
    this.sigmaInstance.graph.addNode({
      id: id1,
      label: '~n0 ^ ~n2 ^ n3',
      x: -30,
      y: -30,
      size: BooleanSat.clauseSize,
      type: 'clauseRenderer',
      action: null,
      status: ClauseStatus.Active,
      settings: {
        drawLabels: false
      }
    })
    this.sigmaInstance.graph.addEdge({
      id: 'c0' + '-' + 'n0',
      source: 'c0',
      target: 'n0',
      type: 'edgeRenderer',
      sign: true
    })
    this.sigmaInstance.graph.addEdge({
      id: 'c0' + '-' + 'n1',
      source: 'c0',
      target: 'n1',
      type: 'edgeRenderer',
      sign: true
    })
    this.sigmaInstance.graph.addEdge({
      id: 'c0' + '-' + 'n2',
      source: 'c0',
      target: 'n2',
      type: 'edgeRenderer',
      sign: false
    })
    this.sigmaInstance.graph.addEdge({
      id: 'c1' + '-' + 'n0',
      source: 'c1',
      target: 'n0',
      type: 'edgeRenderer',
      sign: false
    })
    this.sigmaInstance.graph.addEdge({
      id: 'c1' + '-' + 'n2',
      source: 'c1',
      target: 'n2',
      type: 'edgeRenderer',
      sign: false
    })
    this.sigmaInstance.graph.addEdge({
      id: 'c1' + '-' + 'n3',
      source: 'c1',
      target: 'n3',
      type: 'edgeRenderer',
      sign: true
    })

  }

  public mounted() {

    this.$sigma.canvas.nodes.nodeRenderer = NodeRenderer
    this.$sigma.canvas.nodes.clauseRenderer = ClauseRenderer
    this.$sigma.canvas.edges.edgeRenderer = EdgeRenderer
    this.$sigma.canvas.labels.nodeRenderer = NodeLabelRenderer

    this.createGraph()
    this.sigmaInstance.refresh()

  }

  public beforeUpdate() {
    this.sigmaInstance.refresh()
  }

}
</script>



<style scoped>
.example-graph {
  height: 200px;
  width: 200px;
  margin: auto;
}
</style>
