<template>
<div>

  <h1>DPLL algorithm(<a href="https://en.wikipedia.org/wiki/DPLL_algorithm">wiki</a>)</h1>

  <h2>Naive algorithm</h2>

  <p>How to solve this question? How do we tell if a particular set of nodes and clauses is SAT or not?
    There is a simple way: try every combination of nodes. If we try each combination of nodes and check
    for clause violation, we will either find a set that satisfies or go through every combination and decide UNSAT.
    This brute force approach works, but takes a long time; multiplying by <katex-element expression="2"/> for
    each computation means it takes <katex-element expression="O(2^N)"/> computation time.</p>

  <p>We have a way to solve; but surely we can improve it. Let's turn to the naive DPLL algorithm.</p>

  <h2>To DPLL</h2>

  <p>The DPLL algorithm roots itself on two simple observations: pure literals and unit clauses.
    Both are used to deterministically assign nodes. We will still randomly choose nodes when we
    have to, but we can use these two methods to assign additional nodes beyond our choices.</p>

  <p>Pure literals are simple. If a node is involved in many clauses, it may show up as a true
    literal, <katex-element expression="x_0"/>, or false literal, <katex-element expression="\neg x_0"/>,
    satisfing some clauses if set to <katex-element expression="T"/> and some if set to
    <katex-element expression="F"/>. But it is possible to only show up as one kind of literal.
    If the node only appears as <katex-element expression="x_0"/> in clauses, setting the node
    to <katex-element expression="T"/> satisfies some clauses and setting to <katex-element expression="F"/>
    satisfies none. Vice versa for <katex-element expression="\neg x_0"/>.
    This is a <emph>pure literal</emph>. We gain nothing from setting the node to satisfy no
    clauses, so we might as well choose the node to satisfy many clauses.</p>

  <p>Unit clauses come from having only one way to satisfy a clause. Say we've partially solved
    a SAT problem. As we choose nodes, we can maintain the set of unsatisfied clauses and only check
    what has not already been satisfied. When we set a node to satisfy a clause it appears in, we can remove the
    clause from our unsatisfied set. When we set a node to not satisfy a clause, we can
    <emph>remove the node from the clause</emph>.</p>

  <div v-katex:display="'\\textrm{Clauses before setting }x_0=T'"/>
  <div v-katex:display="'x_0 \\lor x_1 \\lor x_2'"/>
  <div v-katex:display="'\\neg x_0 \\lor x_3 \\lor x_4'"/>
  <div v-katex:display="'x_5 \\lor x_6 \\lor x_7'"/>
  <div v-katex:display="'\\textrm{Clauses after setting }x_0=T'"/>
  <div v-katex:display="'x_3 \\lor x_4'"/>
  <div v-katex:display="'x_5 \\lor x_6 \\lor x_7'"/>

  <p>In this way, some clauses will not have three literals after nodes are assigned. If a clause
    is reduced to <emph>one</emph> literal (a <emph>unit clause</emph>), then the clause can only be satisfied
    by setting that node to make the literal <katex-element expression="T"/>.</p>

  <p>One additional piece of the DPLL algorithm (and generally algorithms that involve random choice):
    backtracking. If during assignment, a clause is reduced to a <emph>null clause</emph> that has
    no literals at all, then it cannot be satisfied. Because we rely on choosing some nodes without knowing
    if the choice is correct, we have to backtrack to choose differently as we encounter these null clauses.
    Naively, we can just backtrack to the last free choice and choose the opposite, declaring UNSAT if there
    is no such choice left. There are many backtracking schemes, and this is one of the primary avenues for
    improvement to the naive DPLL algorithm.</p>

  <h2>Pseudocode</h2>

  <p>The following shows the forward propagating algorithm. The backtracking is excluded
    for simplicity, but essentially a stack of performed actions (or node/clause states) should be reverted
    until a different choice can be made or UNSAT.
    <a href="https://en.wikipedia.org/wiki/DPLL_algorithm">Wikipedia</a> also has pseudocode describing the
    algorithm.</p>

  <pre>
node_assignment = []
unsat_clauses = clauses
while unsat_clauses is non-empty

  actions: queue(AssignNode | CheckNode | CheckClause)
  actions.push(AssignNode(choose_unassigned_node()))
  while !actions.empty
    action = actions.pop()

    // ASSIGN NODE
    if action == AssignNode(n, assign)

      node_assignment.push(n, assign)
      for c in unsat_clauses where c contains n
        if (n, assign) satisfies c
          unsat_clauses.remove(c)
          for n2 in c
            actions.push(CheckNode(n2))
        else
          c.remove((n, assign))
          actions.push(CheckClause(c))

    // PURE LITERAL
    else if action == CheckNode(n)

      assign = undefined
      pure = true
      for c in unsat_clauses where c contains n
        if assign == undefined
          assign = sign(n in c)
        else
          if sign(n in c) != assign
            pure = false
            break

      if assign == undefined
        assign = randomly_choose()

      if pure
        actions.push(AssignNode(n, assign))

    // UNIT / NULL CLAUSE
    else if action = CheckClause(c)

      // breaks out of while, unwinds back to last choice, and restarts while; can return UNSAT
      if size(c) == 0
        BACKTRACK()

      else if size(c) == 1
        (n, assign) = c
        actions.push(AssignNode(n, assign))

return SAT
  </pre>

  <h2>Behavior</h2>

  <p>This algorithm's worst case performance is still <katex-element expression="O(2^N)"/>.
    But this algorithm still performs fairly well for typical problem cases, e.g. randomly
    constructed clauses. And it is a <emph>complete</emph> SAT algorithm, meaning it will
    definitely return either SAT or UNSAT (and it will also return a satisfying assignment if SAT).</p>

  <img :src="timeSvg" :onerror="this.src=timePng" class="plot"/>

  <p>The performance is interesting when looking at the crossover from SAT to UNSAT in random graphs.
    See the above graph. DPLL will tend to quickly find a solution if a problem is easily satisfied.
    But around and beyond the <katex-element expression="4.3"/> transition, the DPLL algorithm will make
    a lot of incorrect choices and need to backtrack. This backtracking drastically increases the time
    required to find SAT or UNSAT. DPLL performs best if it finds a solution with little backtracking
    or finds UNSAT by quickly creating null clauses, early in node assignments. There is a lot of room
    on the right side of the graph for more clauses, so the time required is low below <katex-element expression="4.3"/>,
    quickly increases at the transition, and then has a long, steady tail that is not as high as the
    transition, but not as low as in the SAT region (because it is still difficult to prove UNSAT).</p>

  <p>The other thing to note is the behavior as the number of nodes is increased. The graph is
    <katex-element expression="\mathrm{log}"/> time, so the constant <katex-element expression="y"/> increases
    correspond to an exponential increase in time. This makes sense for an NP-complete problem.</p>

  <h2>Improvements</h2>
    
  <p>I have barely optimized the algorithm in this project and have not put in place any modern tweaks
    to increase performance. The major avenues of improvement and optimization:</p>
  <ul>
    <li>Choosing node assignment</li>
    <li>Backtracking</li>
    <li>Parallelization</li>
  </ul>

  <p>Let's discuss the first two; parallelization is generic enough to skip.</p>

  <p>Node assignment policies can greatly change the performance of the algorithm. An obvious choice
    would be setting a random node to a random assignment. A generally better choice would be setting
    the node that appears with the same sign in the most clauses; setting this node with the appropriate
    sign greedily satisfies the maximum number of clauses. The animation on the main page shows a different
    policy: set the node that is in the most clauses, regardless of its sign. I found it tended to perform
    better than the "max same clauses" policy. This likely is because we are optimizing for when it is
    consistently backtracking; setting the node correctly matters less than picking the node that could
    satisfy a large number of clauses regardless of the final solution branch.</p>

  <p>Backtracking is the deeper optimization. The animation shows naive, or chronological, backtracking.
    If choosing <katex-element expression="x_0=\mathrm{T}"/> shows a null clause then next
    <katex-element expression="x_0=\mathrm{F}"/> is chosen. If that also shows a null clause, we revert
    to the choice before <katex-element expression="x_0"/>, and so on. While this is guaranteed to cover
    the entire search space, it becomes vitally important to choose correctly early on. If the wrong choice
    is made early, we may have to proceed far down the tree before proving it incorrect. As the branching
    gets deeper, it gets wider and computation time shoots up.</p>

  <p>More sophisticated backtracking can drastically improve computation time.
    <a href="https://en.wikipedia.org/wiki/Conflict-driven_clause_learning">Conflict-driven clause learning</a>
    gets an idea for <emph>why</emph> a contradiction was encountered and rearranges the choice tree to more
    quickly prove SAT or UNSAT. Various <a href="https://en.wikipedia.org/wiki/Backjumping">backjumping</a>
    techniques can skip alternate branches, backtracking further without needing to sample the entire
    <katex-element expression="2^N"/> choice space. This repo does not employ any of these improvements</p>

  <p>In addition to improving the DPLL algorithm, we could also try alternatives.
    <a href="https://en.wikipedia.org/wiki/WalkSAT">WalkSAT</a> is one such alternative, which takes motivation
    from <a href="https://en.wikipedia.org/wiki/Simulated_annealing">simulated annealing</a>, searching the
    solution space by asserting a solution and slowly perturbing that solution, watching for the number of
    unsatisfied clauses to decrease. One thing to note when looking at SAT algorithms is the difference between
    complete and incomplete. Complete algorithms are guaranteed to find either SAT or UNSAT. Incomplete may
    not find a solution, e.g. WalkSAT can never <emph>prove</emph> a problem is UNSAT. Given this distinction,
    incomplete algorithms may have greatly improved performance for <emph>probabilistically</emph> solving
    SAT problems.</p>

  <p>Given the importance of the SAT problem, there are many resources to study the problem, including
    archives of difficult problems and benchmarks that generate said problems,
    <a href="http://www.cril.univ-artois.fr/~roussel/satgame/satgame.php?level=4&lang=eng">interactive sites and games</a>,
    and even competitions
    (<a href="https://sat2018.azurewebsites.net/competitions/">1</a>, <a href="http://satcompetition.org">2</a>).
    Make your own solver or SAT problems and participate!</p>

</div>
</template>



<script lang='ts'>
import { Component, Vue } from 'vue-property-decorator'

import timeSvg from '@/assets/timePlot.svg'
import timePng from '@/assets/timePlot.png'


@Component({})
export default class About extends Vue {

  private get timeSvg() { return timeSvg }
  private get timePng() { return timePng }

}
</script>
