const nodeColor: string = '#0f0'
const clauseColor: string = '#373'
const trueColor: string = '#77f'
const falseColor: string = '#f77'
const pruneColor: string = '#333'
const examineColor: string = '#fff'

export enum NodeAction { Nothing, SetTrue, SetFalse }
export enum NodeAssignment { None, True, False }
export enum ClauseAction { Nothing, Prune }
export enum ClauseStatus { Active, Inactive }
export enum EdgeStatus { Active, Inactive }

export function NodeRenderer(node: SigmaJs.Node, context: any, settings: any) {

  const prefix = settings('prefix') || ''
  const size = node[prefix + 'size']

  switch (node.assignment as NodeAssignment) {
  case NodeAssignment.None:
    context.fillStyle = nodeColor
    break
  case NodeAssignment.True:
    context.fillStyle = trueColor
    break
  case NodeAssignment.False:
    context.fillStyle = falseColor
    break
  }

  context.lineWidth = 3
  context.beginPath()
  context.arc(
    node[prefix + 'x'],
    node[prefix + 'y'],
    size,
    0,
    2 * Math.PI
  )
  context.closePath()

  switch (node.action) {
  case null:
    break
  case NodeAction.Nothing:
    context.strokeStyle = examineColor
    context.stroke()
    break
  case NodeAction.SetTrue:
    context.strokeStyle = trueColor
    context.stroke()
    break
  case NodeAction.SetFalse:
    context.strokeStyle = falseColor
    context.stroke()
    break
  }
  context.fill()

}

export function ClauseRenderer(clause: SigmaJs.Node, context: any, settings: any) {

  const prefix = settings('prefix') || ''
  const size = clause[prefix + 'size']

  switch (clause.status as ClauseStatus) {
  case ClauseStatus.Active:
    context.fillStyle = clauseColor
    break
  case ClauseStatus.Inactive:
    context.fillStyle = pruneColor
    break
  }

  context.lineWidth = 3
  context.beginPath()
  context.rect(
    clause[prefix + 'x'] - size,
    clause[prefix + 'y'] - size,
    size * 2,
    size * 2
  )
  context.closePath()

  switch (clause.action) {
  case null:
    break
  case ClauseAction.Nothing:
    context.strokeStyle = examineColor
    context.stroke()
    break
  case ClauseAction.Prune:
    context.strokeStyle = pruneColor
    context.stroke()
    break
  }
  context.fill()

}

export function EdgeRenderer(
  edge: SigmaJs.Edge, source: SigmaJs.Node, target: SigmaJs.Node, context: any, settings: any
) {

  const prefix = settings('prefix') || ''

  if (source.status === ClauseStatus.Active) {
    context.strokeStyle = edge.sign ? trueColor : falseColor
  } else {
    context.strokeStyle = pruneColor
  }

  context.lineWidth = 2
  context.beginPath()
  context.moveTo(source[prefix + 'x'], source[prefix + 'y'])
  context.lineTo(target[prefix + 'x'], target[prefix + 'y'])
  context.closePath()

  context.stroke()

}



export function ChoiceNodeRenderer(node: SigmaJs.Node, context: any, settings: any) {

  const prefix = settings('prefix') || ''
  const size = node[prefix + 'size']

  context.fillStyle = nodeColor

  context.lineWidth = 1
  context.beginPath()
  context.arc(
    node[prefix + 'x'],
    node[prefix + 'y'],
    size,
    0,
    2 * Math.PI
  )
  context.closePath()
  context.fill()

  if (node.contradiction) {
    context.lineWidth = 2
    context.strokeStyle = pruneColor
    context.beginPath()
    context.moveTo(node[prefix + 'x'] - size, node[prefix + 'y'] - size)
    context.lineTo(node[prefix + 'x'] + size, node[prefix + 'y'] + size)
    context.moveTo(node[prefix + 'x'] - size, node[prefix + 'y'] + size)
    context.lineTo(node[prefix + 'x'] + size, node[prefix + 'y'] - size)
    context.closePath()
    context.stroke()
  }

}

export function ChoiceEdgeRenderer(
  edge: SigmaJs.Edge, source: SigmaJs.Node, target: SigmaJs.Node, context: any, settings: any
) {

  const prefix = settings('prefix') || ''

  context.strokeStyle = edge.sign ? trueColor : falseColor

  context.lineWidth = 2
  context.beginPath()
  context.moveTo(source[prefix + 'x'], source[prefix + 'y'])
  context.lineTo(target[prefix + 'x'], target[prefix + 'y'])
  context.closePath()

  context.stroke()

}



export function NodeLabelRenderer(node: SigmaJs.Node, context: any, settings: any) {

  const prefix = settings('prefix') || ''
  const size = node[prefix + 'size']

  if (size < settings('labelThreshold') || !node.label || typeof node.label !== 'string') return

  const fontSize = (settings('labelSize') === 'fixed') ?
    settings('defaultLabelSize') :
    settings('labelSizeRatio') * size

  context.font = (settings('fontStyle') ? settings('fontStyle') + ' ' : '') +
    fontSize + 'px ' + settings('font')
  context.fillStyle = (settings('labelColor') === 'node') ?
    (node.color || settings('defaultNodeColor')) :
    settings('defaultLabelColor')
  context.textAlign = 'center'

  context.fillText(
    node.label,
    Math.round(node[prefix + 'x']),
    Math.round(node[prefix + 'y'] + fontSize / 3)
  )

}
