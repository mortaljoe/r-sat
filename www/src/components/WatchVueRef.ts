import Vue from 'vue'
import { createDecorator, VueDecorator } from 'vue-class-component'


/*
Waits until mount to link a member variable with a template ref

Use as:

  <template>
    ...
    <SomeComponent ref="some-comp-ref"/>
    ...
  </template>

  @Component
  class MyComponent {

    @watchVueRef('some-comp-ref')
    someComponent: SomeComponent  // No initialization required

  }

*/

export default function watchVueRef(refName: string): VueDecorator {
  return createDecorator((options, key) => {
    const data = () => {
      return {
        [key]: null
      }
    }
    const mounted = function(this: Vue) {
      this.$data[key] = this.$refs[refName]
    }
    if (!options.mixins) options.mixins = []
    options.mixins.push({
      data,
      mounted
    })
  })
}
