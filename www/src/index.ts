import Vue, { CreateElement } from 'vue'
import App from './App.vue'
import sigma from 'sigma'
import VueKatex from 'vue-katex'
import 'katex/dist/katex.min.css'

import { initConsolePanic } from 'r-sat'


initConsolePanic()


// PLUGINS
Vue.use(VueKatex)

Vue.prototype.$sigma = sigma


// tslint:disable-next-line no-unused-expression
new Vue({
  el: '#app',
  components: { App },
  render(createElement: CreateElement) {
    return createElement(App)
  }
})
