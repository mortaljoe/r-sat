var path = require('path')
var express = require('express')

var DIST_DIR = path.join(__dirname, '../dist')
var PORT = 3000
var app = express()

// Send index.html when the user access the web
app.get(['/r-sat', '/r-sat/'], function(req, res) {
  res.sendFile(path.join(DIST_DIR, 'index.html'))
})
// Strip /r-sat from the beginning of anything else requested
app.get(['/r-sat/*'], function(req, res) {
  res.sendFile(path.join(DIST_DIR, req.url.substr(6)))
  const noExtension = (path.extname(req.url) === '')
  if(noExtension) res.setHeader('Content-Type', 'text/html')
})

app.listen(PORT)
