const path = require('path')
const webpack = require('webpack')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const htmlTemplate = require('html-webpack-template')
const CopyWebpackPlugin = require('copy-webpack-plugin')

// page is a simple object: { entryName, filename }
function configForPages(pages) {

  const entry = { }
  const plugins  = [ ]
  pages.forEach(page => {
    entry[page.entryName] = [ page.entryFile ]
    plugins.push(new HTMLWebpackPlugin({
      filename: page.filename,
      template: htmlTemplate,
      appMountId: 'app',
      inject: false
    }))
  })

  return {
    plugins,
    entry
  }

}


module.exports = env => {

  const devtool = (env.mode === 'production') ? false : 'inline-source-map'

  const plugins = [
    new VueLoaderPlugin(),
    new CopyWebpackPlugin([
      {
        from: 'config',
        to: '.'
      }
    ])
  ]
  if (env.analyze) plugins.push(new BundleAnalyzerPlugin())

  const vueAlias = (env.mode === 'production') ?
    'vue/dist/vue.runtime.esm.js' :
    'vue/dist/vue.esm.js'

  const pagesConfig = configForPages([
    { entryName: 'index', entryFile: './boot/bootstrap.js', filename: 'index.html' },
    { entryName: 'boolean-sat', entryFile: './boot/bootstrapBooleanSat.js', filename: 'boolean-sat' },
    { entryName: 'dpll', entryFile: './boot/bootstrapDPLL.js', filename: 'dpll' },
    { entryName: 'graph-types', entryFile: './boot/bootstrapGraphTypes.js', filename: 'graph-types' },
    { entryName: 'about', entryFile: './boot/bootstrapAbout.js', filename: 'about' }
  ])

  const entry = pagesConfig.entry
  plugins.push(...pagesConfig.plugins)


  plugins.push(
    new webpack.ProvidePlugin({
      sigma: 'sigma'
    })
  )

  return {
    entry,
    context: path.resolve(__dirname, 'src'),
    mode: env.mode,
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'r-sat.[name].js'
    },
    devServer: {
      publicPath: '/r-sat'
    },
    devtool,
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: {
            loader: 'ts-loader',
            options: {
              appendTsSuffixTo: [/\.vue$/],
              // Since we put this directly on the site, no need for .d.ts
              transpileOnly: true,
            }
          },
          exclude: /node_modules/
        },
        {
          test: /\.vue$/,
          loader: 'vue-loader'
        },
        // this will apply to both plain `.css` files
        // AND `<style>` blocks in `.vue` files
        {
          test: /\.css$/,
          use: [
            'vue-style-loader',
            'css-loader'
          ]
        },
        {
          test: /\.dat$/,
          use: 'file-loader'
        },
        {
          test: /\.(png|jpg|gif|svg)$/i,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 8192
              }
            }
          ]
        },
        {
          test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
          use: [{
              loader: 'file-loader',
              options: {
                  name: '[name].[ext]',
                  outputPath: 'fonts/'
              }
          }]
        },
        {
          test: /sigma.*/,
          use: 'imports-loader?this=>window',
        }
      ]
    },
    optimization: {
      splitChunks: {
        chunks: 'all',
        cacheGroups: { }
      }
    },
    optimization: {
      splitChunks: {
        chunks: 'all',
        cacheGroups: {
          katex: {
            test: /[\\/]node_modules[\\/]katex/,
            name: 'katex',
            chunks: 'all',
            maxSize: 300000
          },
          sigma: {
            test: /[\\/]node_modules[\\/]sigma/,
            name: 'sigma',
            chunks: 'all'
          }
        }
      }
    },
    plugins,
    resolve: {
      symlinks: false,
      alias: {
        '@': path.resolve(__dirname, 'src'),
        vue$: vueAlias
      },
      extensions: [ '.tsx', '.ts', '.js', '.wasm' ]
    }
  }

}
